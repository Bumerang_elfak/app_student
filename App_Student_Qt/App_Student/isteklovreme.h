#ifndef ISTEKLOVREME_H
#define ISTEKLOVREME_H

#include <QDialog>

namespace Ui {
class IstekloVreme;
}

class IstekloVreme : public QDialog
{
    Q_OBJECT

public:
    explicit IstekloVreme(QWidget *parent = 0);
    ~IstekloVreme();


private:
    Ui::IstekloVreme *ui;

};

#endif // ISTEKLOVREME_H
