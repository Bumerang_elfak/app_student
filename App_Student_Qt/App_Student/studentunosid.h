#ifndef STUDENTUNOSID_H
#define STUDENTUNOSID_H
#include "View/view.h"
#include <QMainWindow>
#include "radikviz.h"
#include "sacekajtepitanje.h"
#include <QMessageBox>
namespace Ui {
class StudentUnosID;
}

class StudentUnosID : public View
{
    Q_OBJECT

public:
    explicit StudentUnosID(QWidget *parent = 0);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
        connect(controller, SIGNAL(refresh_logovanje(Odgovor_na_Komandu*)),this,SLOT(odgovor_na_logovanje(Odgovor_na_Komandu*)));
        connect(controller, SIGNAL(signal_greska(Error*)),this,SLOT(odgovor_error(Error*)));
        connect(this->controller,SIGNAL(signal_error_diskonektovan_sa_servera()),this,SLOT(odgovor_diskonektovan()));
    }

    ~StudentUnosID();

private slots:
    void on_zapocniKvizButton_clicked();

    void on_IDEdit_returnPressed();

private:
    Ui::StudentUnosID *ui;
    SacekajtePitanje* sacekajte_pit;

public slots:
    void odgovor_na_logovanje(Odgovor_na_Komandu *odg);
    void odgovor_error(Error* greska);
    void odgovor_diskonektovan();




};

#endif // STUDENTUNOSID_H
