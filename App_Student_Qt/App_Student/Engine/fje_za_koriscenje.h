#ifndef FJE_ZA_KORISCENJE_H
#define FJE_ZA_KORISCENJE_H

#include <QString.h>
#include <QObject.h>
#include "Engine/Enums.h"


//#include "Odgovor_na_komandu/odgovor_na_komandu.h"

/*
#include "Osobe/osobe.h"
#include "Osobe/profesor.h"
#include "Osobe/admin.h"
#include "Osobe/student.h"
*///xD ovo ne radi

/*
#include "PPOK/kviz.h"
#include "PPOK/oblast.h"
#include "PPOK/pitanje.h"
#include "PPOK/ppok.h"
#include "PPOK/predmet.h"
*///i obo ne radi xD
//i ovo ne radi! ne znam zasto

//#include "Komanda/Komanda.h" //ovo samo po sebi izbacuje gresku

namespace Fje_za_Koriscenje{

// vraca br od poz broj, a broj
// se postavlja na kraj broja iz stringa!
int vrati_broj(QString s, int &broj);

//radi samo za br>=0
int broj_cifara_broja(int br);

//procitaj iz <> i enkapsuliraj u <>
//dekapsulacija i ako se u stringu s nalaze <> neki iznutra
//ce zanemariti te iznutra
QString dekapsulacija( QString s, int &broj );
QString enkapsuliraj(QString s);

//procitaj n karaktera iz String
QString procitaj_n(QString s, int &broj);

QString enkapsulirajZaBazu(QString s);
QString enkapsulirajZaBazu(int s);
QString enkapsulirajZaBazu(bool s);
}

#endif // FJE_ZA_KORISCENJE_H
