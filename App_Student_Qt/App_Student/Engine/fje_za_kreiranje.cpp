#include "Engine/fje_za_kreiranje.h"





namespace Fje_za_kreiranje_objekata{

using namespace Fje_za_Koriscenje;

Komanda* pravi_Komandu(QString izvorniString){
    int n=0;

    QString tipKomande;

    Komanda *novaKomanda=NULL;
    tipKomande=dekapsulacija(izvorniString,n);

    int tip=tipKomande.toInt();
    switch(tip)
    {
    case KOMANDA_VRATI_SVE_AKTIVNE_KVIZOVE:
    {
        novaKomanda=new komanda_vrati_sve_aktivne_kvizove();
        novaKomanda->praviOd(izvorniString);
        break;
    }
    case KOMANDA_VRATI_SVE_LOGOVANE_PROFESORE:
    {
        novaKomanda=new komanda_vrati_sve_logovane_profesore();
        novaKomanda->praviOd(izvorniString);
        break;
    }
    //Specialne komande
    case KOMANDA_GENERISI_KVIZ_DINAMICKI:
    {
        novaKomanda=new Komanda_Generisi_Kviz_Dinamicki();
        novaKomanda->praviOd(izvorniString);
        break;
    }
    case KOMANDA_LOGOVANJE:
    {
        novaKomanda=new Komanda_Logovanje();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    //KOMANDA_KVIZ
    case KOMANDA_KVIZ_AKTIVIRAJ_KVIZ:
    {
        novaKomanda=new Komanda_Kviz_Aktiviraj();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_KVIZ_ISTEKLO_VREME:
    {
        novaKomanda=new Komanda_isteklo_vreme();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_KVIZ_LOGUJ_SE_NA_KVIZ:
    {
        novaKomanda=new Komanda_Kviz_Log();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_KVIZ_NASILAN_PREKID:
    {
        novaKomanda=new Komanda_Kviz_Nasilno_Prekini();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_KVIZ_NORMALAN_PREKID:
    {
        novaKomanda=new Komanda_Kviz_Normalan_Prekid();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_KVIZ_ODGOVOR_NA_PITANJE:
    {
        novaKomanda=new Komanda_Kviz_Odg_Pitanje();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_KVIZ_PRIKAZI_STATISTIKU:
    {
        novaKomanda=new Komanda_Kviz_Prikazi_Statistiku();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_KVIZ_SLEDECE_PITANJE:
    {
        novaKomanda=new Komanda_Kviz_sl_Pitanje();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    //KOMANDA_NAD_BAZOM
    case KOMANDA_MODIFIKUJ:
    {
        int tabela=dekapsulacija(izvorniString,n).toInt();
        switch(tabela)
        {
        case ADMIN:
        {
            novaKomanda=new komandamodifikuj_admin();
            novaKomanda->praviOd(izvorniString);
            break;
        }
        case KVIZ:
        {
            novaKomanda=new komandamodifikuj_kviz();
            novaKomanda->praviOd(izvorniString);
            break;
        }
        case OBLAST:
        {
            novaKomanda=new komandamodifikuj_oblast();
            novaKomanda->praviOd(izvorniString);
            break;
        }
        case PITANJE:
        {
            novaKomanda=new komandamodifikuj_pitanje();
            novaKomanda->praviOd(izvorniString);
            break;
        }
        case PREDMET:
        {
            novaKomanda=new komandamodifikuj_predmet();
            novaKomanda->praviOd(izvorniString);
            break;
        }
        case PROFESOR:
        {
            novaKomanda=new komandamodifikuj_profesor();
            novaKomanda->praviOd(izvorniString);
            break;
        }
        case STUDENT:
        {
            novaKomanda=new komandamodifikuj_student();
            novaKomanda->praviOd(izvorniString);
            break;
        }

        }//drugi switch

        return novaKomanda   ;

    }
    case KOMANDA_MODIFIKUJ_CELO_PITANJE:{
        novaKomanda=new Komanda_Modifikuj_celo_Pitanje();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_MODIFIKUJ_CEO_KVIZ:{
        novaKomanda=new Komanda_Modifikuj_Ceo_Kviz();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_BRISI:
    {
        novaKomanda=new KomandaBrisi();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_DODAJ:
    {
        int tabela=dekapsulacija(izvorniString,n).toInt();
        switch(tabela)
        {
        case ADMIN:
        {
            novaKomanda=new komandadodaj_admin();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        case KVIZ:
        {
            novaKomanda=new komandadodaj_kviz();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        case OBLAST:
        {
            novaKomanda=new komandadodaj_oblast();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        case PITANJE:
        {
            novaKomanda=new komandadodaj_pitanje();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        case PREDMET:
        {
            novaKomanda=new komandadodaj_predmet();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        case PROFESOR:
        {
            novaKomanda=new komandadodaj_profesor();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        case STUDENT:
        {
            novaKomanda=new komandadodaj_student();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        case POSEDUJE_PITANJE:
        {
            novaKomanda=new komandadodaj_poseduje_pitanje();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
        }
        return novaKomanda;
    }
    case KOMANDA_UPIT_PROFESOR_BEZ_PREDMETA:{
            novaKomanda=new komandaupit_profesori_bez_predmeta();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
    case KOMANDA_UPIT_KVIZOVI_ZA_PITANJE:{
        novaKomanda=new komandaupit_kvizovi_za_pitanja();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_UPIT_PITANJA_ZA_KVIZ :{
            novaKomanda=new komandaupit_pitanja_za_kviz();
            novaKomanda->praviOd(izvorniString);
            return novaKomanda;
        }
    case KOMANDA_UPIT_PROFESOR_SVE:{
        novaKomanda=new KomandaUpit_Sve_za_Profesora();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    case KOMANDA_DODAJ_CEO_KVIZ:{
        novaKomanda = new Komandadodaj_ceo_kviz();
        novaKomanda->praviOd(izvorniString);
        return novaKomanda;
    }
    default:{//  KOMANDE_NAD_BAZOM_UPITI

    //  Komanda_UPIT OVO MOZE OVAKO JER SE KOMANDA LOGOVANJE SREDI IZNAD
    //  I KOMANDA_GENERISI KVIZ DINAMICKI ISTO TAKO!!!!
    //  PA OVDE OSTAJE SKUP KOMANDI KOJE SU NAD BAZOM...

        if( tip>=KOMANDA_UPIT && tip<=KOMANDA_UPIT_PROFESOR_BEZ_PREDMETA ){
            int tabela=dekapsulacija(izvorniString,n).toInt();
            switch(tabela)
            {
            case ADMIN:
            {
                novaKomanda=new komandaupit_admin();
                novaKomanda->praviOd(izvorniString);
                return novaKomanda;
            }
            case KVIZ:
            {
                novaKomanda=new komandaupit_kviz();
                novaKomanda->praviOd(izvorniString);
                return novaKomanda;
            }
            case OBLAST:
            {
                novaKomanda=new komandaupit_oblast();
                novaKomanda->praviOd(izvorniString);
                return novaKomanda;
            }
            case PITANJE:
            {
                novaKomanda=new komandaupit_pitanje();
                novaKomanda->praviOd(izvorniString);
                return novaKomanda;
            }
            case PREDMET:
            {
                novaKomanda=new komandaupit_predmet();
                novaKomanda->praviOd(izvorniString);
                return novaKomanda;
            }
            case PROFESOR:
            {
                novaKomanda=new komandaupit_profesor();
                novaKomanda->praviOd(izvorniString);
                return novaKomanda;
            }
            case STUDENT:
            {
                novaKomanda=new komandaupit_student();
                novaKomanda->praviOd(izvorniString);
                return novaKomanda;
            }
            }//switch
        }//if
    }
    }

    return novaKomanda;
}
Odgovor_na_Komandu* pravi_Odgovor_na_komandu(QString izvorniString)
{
    int n=0;
    Odgovor_na_Komandu *o=NULL;
    QString pom;
    pom=dekapsulacija(izvorniString,n);//u pom je tip odgovora
    int tip=pom.toInt();

    switch(tip)
    {
    //ODGOVOR ERROR
    case ERROR:{
        o=new Error();
        o->praviOd(izvorniString);
        return o;
    }
    case ERROR_DISKONEKTOVAN_SA_SERVERA:{
        o=new error_diskonektovan_sa_servera();
        o->praviOd(izvorniString);
        return o;
    }
    //ODGOVOR_KVIZ
    case ODGOVOR_KVIZ_AKTIVAN_KVIZ:
    {
        o=new Odgovor_Aktivan_Kviz();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_ISTEKLO_VREME:
    {
        o=new Odgovor_Isteklo_Vreme();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_SLEDECE_PITANJE:
    {
        o=new Odgovor_sledece_Pitanje();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_SVI_ODGOVORILI:
    {
        o=new Odgovor_Svi_Odgovorili();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_TREN_STATISTIKA_PITANJA:
    {
        o=new Odgovor_tren_statistika_pitanja();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_USPESNA_PRIJAVA:
    {
        o=new Odgovor_Uspesna_Prijava();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_USPESNO_ZAVRSEN_KVIZ:
    {
        o=new Odgovor_Uspesno_zavrsen_Kviz();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_NASILAN_PREKID:
    {
        o=new Odgovor_nasilan_Prekid();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_KVIZ_CEKAJ_SLEDECE_PITANJE:{
        o=new odgovor_cekaj_sledece_pitanje();
        o->praviOd(izvorniString);
        return o;
    }
        //ODGOVOR_SA_MOD_BAZE
    case ODGOVOR_MODIFIKOVAN:
    {
        o=new Odgovor_Modifikovan();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_OBRISAN:
    {
        o=new Odgovor_Obrisan();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_SNIMLJEN:
    {
        o=new Odgovor_Snimljen();
        o->praviOd(izvorniString);
        return o;
    }

        //ODGOVOR_SA_PODACIMA
    case ODGOVOR_KVIZOVI:
    {
        o=new Odgovor_Kvizovi();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_OBLASTI:
    {
        o=new Odgovor_Oblasti();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_PITANJA:
    {
        o=new Odgovor_Pitanja();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_PREDMETI:
    {
        o=new Odgovor_Predmeti();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_PROFESORI:
    {
        o=new Odgovor_Profesori();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_GENERISI_KVIZ_DINAMICKI:
    {
        o=new odgovor_generisi_kviz_dinamicki();
        o->praviOd(izvorniString);
        return o;
    }
    case ODGOVOR_LOGOVANJE:
    {
        o=new odgovor_logovanje();
        o->praviOd(izvorniString);
        return o;
    }
    }


    return o;
}

}
