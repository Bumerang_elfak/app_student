#ifndef KOMUNIKACIJA_SA_SERVEROM_WORKER_H
#define KOMUNIKACIJA_SA_SERVEROM_WORKER_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>
#include <QHostAddress>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"
#include "Engine/fje_za_kreiranje.h"
#include "Engine/Odgovor_na_komandu/error_diskonektovan_sa_servera.h"
#include <iostream>


class Komunikacija_sa_serverom_Worker : public QObject
{
    Q_OBJECT

    QThread                 Worker;
    int                     radi;   //0-nista, 1-slanje, 2-primanje
    int                     port;
    QString                 ime_error_fajla;
    int                     broj_poruka;
    int                     duzina_tren_poruke=-1;
    QString                 IP;
    QTcpSocket              *sock;
    Komanda                 *komanda;
    Odgovor_na_Komandu      *odgovor;

public:
    explicit Komunikacija_sa_serverom_Worker(QObject *parent = 0);
    ~Komunikacija_sa_serverom_Worker();

protected:
    void run();

signals:
    void stigo_odg(Odgovor_na_Komandu* odgovor);

public slots:
    void slot_pravi_soket();
    void slot_diskonekcija();
    void prihvati_od_soketa();
    void salji_komandu( Komanda* k );
};

#endif // KOMUNIKACIJA_SA_SERVEROM_WORKER_H
