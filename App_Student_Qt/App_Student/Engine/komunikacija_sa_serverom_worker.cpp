#include "komunikacija_sa_serverom_worker.h"

//int radi;   //0-nista, 1-slanje, 2-primanje
//int port;
//QString IP;
//QTcpSocket sock;
//Komanda *komanda;
//Odgovor_na_Komandu *odgovor;

Komunikacija_sa_serverom_Worker::Komunikacija_sa_serverom_Worker(QObject *parent) : QObject(parent){
    port=8888;
    IP="127.0.0.1";
    broj_poruka=0;
    komanda=NULL;
    odgovor=NULL;
    ime_error_fajla="Errors";
    std::cout<<"Rodio sam se ali nisam napravio socket"<<std::endl;
}
void Komunikacija_sa_serverom_Worker::slot_pravi_soket(){
    QHostAddress adresa;
    adresa.setAddress(IP);
    sock=new QTcpSocket();
    sock->connectToHost(adresa,port);
    if( !sock->isOpen() ){
        Error *e=new error_diskonektovan_sa_servera();
        e->postavi_greska("Proverite internet konekciju!!!");
        emit stigo_odg(e);
    }
    connect(sock, SIGNAL(readyRead()),
            this, SLOT(prihvati_od_soketa()),
            Qt::QueuedConnection);
    connect(sock,SIGNAL(disconnected()),
            this,SLOT(slot_diskonekcija()),
            Qt::QueuedConnection);
    std::cout<<"Rodio se i soket"<<std::endl;
}
Komunikacija_sa_serverom_Worker::~Komunikacija_sa_serverom_Worker(){
    delete komanda;
    delete odgovor;
    sock->abort();
    std::cout<<"Umro sam"<<std::endl;
}

void Komunikacija_sa_serverom_Worker::run(){
        //mucak
        if( radi==0 ){
            return;
        }

        if( !sock->isOpen() ||
                sock->state()==QAbstractSocket::SocketState::UnconnectedState){
            Error *e=new Error();
            e->postavi_greska("Proverite konekciju sa internetom");
            emit stigo_odg(e);
            return;
        }

        //slanje komande
        if( radi==1 ){
            komanda->SaljiNaPort(sock);
            QFile f(ime_error_fajla + QString::number(broj_poruka) + QString(".txt"));
            f.open(QIODevice::WriteOnly | QIODevice::Text);
            QString poruka;
            poruka.append(komanda->za_slanje());
            if( f.isOpen() ){
                QTextStream stream(&f);
                poruka.append("/n");
                poruka.append(QDateTime::currentDateTime().toString());
                poruka.append("\n\n");
                stream << poruka;
            }
            f.close();

            delete komanda;
            komanda=NULL;
            return;
        }

        //primanje od sock-a
        if( radi==2 ){
            QByteArray ba;

            //ne mogu da procitam duzinu!!!

            if( duzina_tren_poruke==-1 ){//znaci da poruka ne postoji
                //ne mogu da procitam duzinu!!!
                if( sock->bytesAvailable()<sizeof(int) )
                    return;
                sock->read((char *)&duzina_tren_poruke,sizeof(int));
            }

            //cekamo da stigne cela poruka
            if(sock->bytesAvailable() < duzina_tren_poruke){
                qDebug()<<"Cekamo da stigne znam duzinu";
                return;
            }
                /*
            {
                //return;
                try
                {
                    sock->waitForReadyRead();//namerno neka saceka klijent
                }
                catch(...)
                {
                    //nesto ne valja!!! eventualno izbaciti gresku neku!!!
                }
            }
            */

            //sve je stiglo sada treba da se pravi obj
            char *podaci=new char[duzina_tren_poruke+1];
            sock->read(podaci,duzina_tren_poruke);
            podaci[duzina_tren_poruke]='\0';    //SVI PODACI SU TEKSTUALNI

            //u podaci mi se nalazi cela poruka
            QString poruka;
            poruka.append(podaci);
            delete []podaci;    //podaci se brisu jer nisu vise potrebni

            Odgovor_na_Komandu *odgovor=Fje_za_kreiranje_objekata::pravi_Odgovor_na_komandu(poruka);
            //ZAPAZITI OVO OVDE MOZE ODG DA BUDE NULL
            if( odgovor==NULL ){
                Error *e=new Error();
                e->postavi_greska("Stiglo je nesto ali ne moze da se dekodira OPASNA GRESKA");
                odgovor=e;
            }
            emit stigo_odg(odgovor);

            QFile f(ime_error_fajla + QString::number(broj_poruka) + QString(".txt"));
            if( odgovor==NULL ){
                f.open(QIODevice::Append | QIODevice::Text);
                if( f.isOpen() ){
                    QTextStream stream(&f);
                    poruka.append("/n");
                    poruka.append(QDateTime::currentDateTime().toString());
                    poruka.append("\n\n");
                    stream << poruka;
                }
                f.close();
            }
            else
                f.remove();

            duzina_tren_poruke=-1;
            broj_poruka++;
            return;
        }
        //Worker.wait();
}

void Komunikacija_sa_serverom_Worker::prihvati_od_soketa(){
    radi=2;
    run();
}

void Komunikacija_sa_serverom_Worker::slot_diskonekcija(){
    Error *e=new error_diskonektovan_sa_servera();
    e->postavi_greska("Diskonektovali ste se sa servera!");
    emit stigo_odg(e);
}

void Komunikacija_sa_serverom_Worker::salji_komandu(Komanda *k ){
    radi=1;
    komanda=k;
    run();
}
