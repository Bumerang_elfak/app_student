#include "Engine/PPOK/predmet.h"



//constrictors & destructors
Predmet::Predmet()
{
    tip=PREDMET;
}
Predmet::~Predmet(){
    oblasti.clear();
}

//functions
//public
bool Predmet::dodaj_oblast(const Oblast& o){
    oblasti.append(o);
    return true;
}
bool Predmet::izbrisi_oblast(const Oblast& o){
    int i=oblasti.indexOf(o);
    if( i==-1 )
        return false;
    oblasti.removeAt(i);
    return true;
}


QByteArray Predmet::za_slanje(){

    // Tip || ID || ID_PROF || ime || br_o || oblasti
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(tip)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_Baze)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Profesor)));
    stringZaSlanje.append(enkapsuliraj(ime));
    int n=oblasti.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(n)));
    for( int i=0; i<n; i++ )
    {
        stringZaSlanje.append(enkapsuliraj(oblasti[i].za_slanje()));
    }
    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}
int Predmet::pravi_od(QString &s){

    // Tip || ID || ID_PROF || ime || br_o || oblasti
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if( pomocna.toInt()!=PREDMET )
        return 0;

    pomocna=dekapsulacija(s,iterator);
    ID_iz_Baze=pomocna.toInt();

    pomocna=dekapsulacija(s,iterator);
    ID_iz_baze_Profesor=pomocna.toInt();

    pomocna=dekapsulacija(s,iterator);
    ime=pomocna;

    pomocna=dekapsulacija(s,iterator);
    int n=pomocna.toInt();
    for( int i=0; i<n; i++ ){
        Oblast o;
        QString ba;
        ba=dekapsulacija(s,iterator);
        int duzina=o.pravi_od(ba);
        if( duzina==0 )
            return 0;
        dodaj_oblast(o);
    }
    return iterator;
}

bool Predmet::operator==(const Predmet &p)
{
   if(p.ID_iz_Baze!=ID_iz_Baze)
        return false;
    return true;
}

Predmet& Predmet::operator=(const Predmet &p)
{
    tip=p.tip;
    ime=p.ime;
    ID_iz_Baze=p.ID_iz_Baze;
    ID_iz_baze_Profesor=p.ID_iz_baze_Profesor;
    return *this;
}

