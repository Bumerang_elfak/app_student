#ifndef PREDMET_H
#define PREDMET_H

//GRESKE POGLEDATI PRILIKOM PROTOKOLA

#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/oblast.h"
#include <QString>
#include <QList>



class Predmet : public PPOK
{
    Q_OBJECT
private:
    QList<Oblast> oblasti;
    int ID_iz_baze_Profesor;

public:
    Predmet();
    ~Predmet();

    QList<Oblast>   vrati_oblasti(){
        return oblasti;
    }
    QList<Oblast>*  vrati_pointer_na_oblasti(){
        return &oblasti;
    }

    bool dodaj_oblast(const Oblast& o);
    bool izbrisi_oblast(const Oblast& o);
    bool izbrisi_oblast_id(int id){
        for( int i=0; i<oblasti.count(); i++ )
            if( oblasti[i].vrati_ID()==id ){
                oblasti.removeAt(i);
                i--;
                return true;
            }
        return false;
    }
    bool izbrisi_pitanje(int id_oblasti,int id_pitanja){
        bool pov;
        for( int i=0; i<oblasti.count(); i++ )
            if( oblasti[i].vrati_ID()==id_oblasti ){
                pov+=oblasti[i].brisi_pitanje(id_pitanja);
            }
        return pov;
    }
    bool izbrisi_kviz(int id_oblasti,int id_kviza){
        for( int i=0; i<oblasti.count(); i++ )
            if( oblasti[i].vrati_ID()==id_oblasti ){
                return oblasti[i].brisi_kviz(id_kviza);
            }
        return false;
    }
    void modifikuj_kviz( Kviz k ){
        for( int i=0; i<oblasti.count(); i++ )
            if( oblasti[i].vrati_ID()==k.vrati_ID_Oblasti() )
                oblasti[i].modifikuj_kviz(k);
    }
    void nadovezi_listu_oblasti( QList<Oblast> n_o ){
        oblasti.append(n_o);
    }
    int broj_oblasti(){
        return oblasti.count();
    }
    Oblast& vrati_oblast_na( int i ){
        return oblasti.operator [](i);
    }

    void postavi_ID_Profesora(int noviID)
    {
        ID_iz_baze_Profesor=noviID;
    }
    int vrati_ID_Profesora()
    {
        return ID_iz_baze_Profesor;
    }


    virtual QByteArray      za_slanje();
    virtual int             pravi_od(QString &a);

    bool operator==(const Predmet &p);
    Predmet& operator=(const Predmet &p);
};



#endif // PREDMET_H
