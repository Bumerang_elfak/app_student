#include "Engine/PPOK/oblast.h"

//constrictors & destructors
Oblast::Oblast()
    :PPOK(){

    tip=OBLAST;
}
Oblast::Oblast(const Oblast& o)
    :PPOK(o){
    pravi_od(o);
}
Oblast::~Oblast(){
    brisi_mem();
}

//functions
//private
void Oblast::brisi_mem(){
    kvizovi.clear();
    pitanja.clear();
}
void Oblast::pravi_od(const Oblast& o){
    kvizovi=o.kvizovi;
    pitanja=o.pitanja;
    ID_iz_Baze=o.ID_iz_Baze;
    ID_iz_baze_Predmet=o.ID_iz_baze_Predmet;
    ID_iz_baze_Profesor=o.ID_iz_baze_Profesor;
    ime=o.ime;
    tip=o.tip;
}

//public
bool        Oblast::dodaj_kviz(const Kviz& k){
    kvizovi.append(k);
    return true;
}
bool        Oblast::dodaj_pitanje(const Pitanje &p){
    pitanja.append(p);
    return true;
}
bool        Oblast::izbaci_kviz(const Kviz& k){
    int i=kvizovi.indexOf(k);
    if( i==-1 )
        return false;
    kvizovi.removeAt(i);
    return true;
}
bool Oblast::izbaci_pitanje(const Pitanje &p){
    int i=pitanja.indexOf(p);
    if( i==-1 )
        return false;
    pitanja.removeAt(i);
    return true;
}
QByteArray Oblast::za_slanje(){

    // tip || ID || ID_iz_baze_Profesor || ID_iz_baze_Predmet || ime || br_k || kvizovi || br_p || pitanja
    using namespace Fje_za_Koriscenje;


    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(tip)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_Baze)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Profesor)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Predmet)));
    stringZaSlanje.append(enkapsuliraj(ime));

    int n=kvizovi.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(n)));
    for( int i=0; i<n; i++ )
        stringZaSlanje.append(enkapsuliraj(kvizovi[i].za_slanje()));

    n=pitanja.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(n)));
    for( int i=0; i<n; i++ )
    {
        stringZaSlanje.append(enkapsuliraj(pitanja[i].za_slanje()));
    }
    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;

}
int Oblast::pravi_od(QString &s){

    // tip || ID || ID_iz_baze_Profesor || ID_iz_baze_Predmet || ime || br_k || kvizovi || br_p || pitanja
    using namespace Fje_za_Koriscenje;

    brisi_mem();
    int iterator=0;
    QString pomocna;


    pomocna=dekapsulacija(s,iterator);
    if( pomocna.toInt()!=OBLAST )
        return 0;
    //std::cout<<"Tip "<<pomocna.toStdString()<<std::endl;

    pomocna=dekapsulacija(s,iterator);
    ID_iz_Baze=pomocna.toInt();
    //std::cout<<"Id iz baze "<<pomocna.toStdString()<<std::endl;
    pomocna=dekapsulacija(s,iterator);
    ID_iz_baze_Profesor=pomocna.toInt();
    pomocna=dekapsulacija(s,iterator);
    ID_iz_baze_Predmet=pomocna.toInt();
    pomocna=dekapsulacija(s,iterator);
    ime=pomocna;
    //std::cout<<"Ime "<<pomocna.toStdString()<<std::endl;

    int n;
    pomocna=dekapsulacija(s,iterator);
    //std::cout<<"Broj kvizova "<<pomocna.toStdString()<<std::endl;
    n=pomocna.toInt();
    for( int i=0; i<n; i++ ){
        Kviz k;
        QString ba;
        ba.append(dekapsulacija(s,iterator));
        //std::cout<<pomocna.toStdString()<<std::endl;
        int duzina=k.pravi_od(ba);
        if( duzina==0 )
            return 0;
        dodaj_kviz(k);
    }

    pomocna=dekapsulacija(s,iterator);
    //std::cout<<"Broj pitanja "<<pomocna.toStdString()<<std::endl;
    n=pomocna.toInt();
    for( int i=0; i<n; i++ ){
        Pitanje p;
        QString ba;
        ba.append(dekapsulacija(s,iterator));
        //std::cout<<pomocna.toStdString()<<std::endl;
        int duzina=p.pravi_od(ba);
        if(duzina==0)
            return 0;
        //iterator+=duzina;
        dodaj_pitanje(p);
    }
    return iterator;
}

//operators
bool Oblast::operator==(const Oblast& o){
    if( ID_iz_Baze==o.ID_iz_Baze )
        return true;
    return false;
}
Oblast& Oblast::operator=(const Oblast& o){
    brisi_mem();
    pravi_od(o);
    return *this;
}

