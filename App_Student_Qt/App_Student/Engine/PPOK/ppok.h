#ifndef PPOK_H
#define PPOK_H

//GRESKE POGLEDATI PRILIKOM PROTOKOLA

#include <QObject>
#include <string.h>
#include <QString.h>
#include <QTcpSocket>
#include "Engine/Enums.h"
#include <iostream>

class PPOK : public QObject
{
    Q_OBJECT
public:
    explicit PPOK(QObject *parent = 0);
    PPOK(const PPOK& p);
    virtual ~PPOK();

protected:
    QString         ime="";
    Tipovi          tip;
    int             ID_iz_Baze=-1;

public:
    int                     vrati_ID();
    void                    postavi_ID(int ID);
    int                     vrati_tip();
    QString                 vrati_ime();
    bool                    postavi_ime(QString ime);
    void                    salji_na_port(QTcpSocket *sock){
        sock->write(za_slanje());
    }
    virtual  QString        toString();

    virtual QByteArray      za_slanje()=0;
    virtual int             pravi_od(QString &a)=0;
};


#endif // PPOK_H
