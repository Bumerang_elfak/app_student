#include "Engine/PPOK/kviz.h"


//constrictors & destructors
Kviz::Kviz()
    :PPOK(){
    tip=KVIZ;
    tezina=-1;
}
Kviz::Kviz(const Kviz& k)
    :PPOK(k){
    Kviz();
    pravi_od(k);
}
Kviz::~Kviz(){
    brisi_mem();
}

//functions
//private
void Kviz::brisi_mem(){
    pitanja.clear();
}
void Kviz::pravi_od(const Kviz& k){
    if( k.tip!=KVIZ )
        return;
    ID_iz_Baze=k.ID_iz_Baze;
    ime=k.ime;
    tezina=k.tezina;
    pitanja=k.pitanja;
    ID_iz_baze_Oblast=k.ID_iz_baze_Oblast;
    ID_iz_baze_Predmet=k.ID_iz_baze_Predmet;
    ID_iz_baze_Profesor=k.ID_iz_baze_Profesor;
    izracunaj_tezinu();
}

//public
bool        Kviz::dodaj_pitanje( Pitanje& pit ){
    pitanja.append(pit);
    return true;
}
bool        Kviz::izbaci_pitanje( Pitanje& pit ){
    int i=pitanja.indexOf(pit,0);
    if( i==-1 )
        return false;
    pitanja.removeAt(i);
    return true;
}
bool        Kviz::izbaci_pitanje( int id ){
    int i;
    for( i=0; i<pitanja.count(); i++ )
        if( pitanja[i].vrati_ID()==id )
            break;
    if( i!=pitanja.count() ){
        pitanja.removeAt(i);
        return true;
    }
    return false;
}
int         Kviz::vrati_tezinu(){
    if( pitanja.count()==0 )
        return tezina;
    if( tezina!=-1 )
        return tezina;
    izracunaj_tezinu();
    return tezina;
}
void        Kviz::izracunaj_tezinu(){
    int n=pitanja.length();
    int suma=0;
    if( n==0 ){
        return;
    }
    for( int i=0; i<n; i++ )
        suma+=pitanja[i].vrati_tezinu();
    tezina=suma/n;

}
QByteArray Kviz::za_slanje(){
    // TIP || ID || ID_iz_baze_Profesor ||
    // ID_iz_baze_Predmet || ID_iz_baze_Oblast || IME || br_pitanja || pitanja
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(tip)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_Baze)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Profesor)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Predmet)));
    stringZaSlanje.append(enkapsuliraj(QString::number(ID_iz_baze_Oblast)));
    stringZaSlanje.append(enkapsuliraj(ime));

    int n=pitanja.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(n)));
    for( int i=0; i<n; i++ )
    {
        stringZaSlanje.append(enkapsuliraj(pitanja[i].za_slanje()));
    }
    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;

}
int Kviz::pravi_od(QString &s){

    // TIP || ID || ID_iz_baze_Profesor ||
    // ID_iz_baze_Predmet || ID_iz_baze_Oblast || IME || br_pitanja || pitanja
    using namespace Fje_za_Koriscenje;

    brisi_mem();
    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=KVIZ)
        return 0;

    pomocna=dekapsulacija(s,iterator);
    postavi_ID(pomocna.toInt());

    pomocna=dekapsulacija(s,iterator);
    postavi_ID_Profesora(pomocna.toInt());
    pomocna=dekapsulacija(s,iterator);
    postavi_ID_Predmeta(pomocna.toInt());
    pomocna=dekapsulacija(s,iterator);
    postavi_ID_Oblasti(pomocna.toInt());

    pomocna=dekapsulacija(s,iterator);
    postavi_ime(pomocna);

    pomocna=dekapsulacija(s,iterator);
    int n=pomocna.toInt();
    for( int i=0; i<n; i++ ){
        Pitanje p;
        QString ba;
        ba.append(dekapsulacija(s,iterator));
        int duzina=p.pravi_od(ba);
        if( duzina==0 )
            return 0;
        dodaj_pitanje(p);
    }
    return iterator;
}

//operators
Kviz& Kviz::operator=(const Kviz& k){
    brisi_mem();
    pravi_od(k);
    return *this;
}
bool Kviz::operator==(const Kviz& k){
    if( ID_iz_Baze==k.ID_iz_Baze )
        return true;
    return false;

}

