#ifndef PITANJE_H
#define PITANJE_H

//GRESKE POGLEDATI PRILIKOM PROTOKOLA


#include <QString>
#include <QByteArray>
#include "Engine/PPOK/ppok.h"
#include "Engine/fje_za_koriscenje.h"



class Pitanje : public PPOK
{
    Q_OBJECT

private:


    QList<QString>      odgovori;
    int         tacan=-1;
    int         br_tren_odg=-1;
    int         tezina=-1;

    int ID_iz_baze_Profesor;
    int ID_iz_baze_Predmet;
    int ID_iz_baze_Oblast;

    void        oduzmi_mem();
    void        pravi_od(const Pitanje& p);
public:
    Pitanje();
    Pitanje(const Pitanje& p);
    ~Pitanje();


    void postavi_ID_Profesora(int noviID)
    {
        ID_iz_baze_Profesor=noviID;
    }
    int vrati_ID_Profesora()
    {
        return ID_iz_baze_Profesor;
    }
    void postavi_ID_Predmeta(int noviID)
    {
        ID_iz_baze_Predmet=noviID;
    }
    int vrati_ID_Predmeta()
    {
        return ID_iz_baze_Predmet;
    }
    void postavi_ID_Oblasti(int noviID)
    {
        ID_iz_baze_Oblast=noviID;
    }
    int vrati_ID_Oblasti()
    {
        return ID_iz_baze_Oblast;
    }

    QString             vrati_tekst();
    void                obrisi_odgovore(){
        odgovori.clear();
    }
    bool                postavi_tekst(QString t);
    bool                dodaj_odg(QString odg);
    QList<QString>      vrati_odgovore();
    void                postavi_tacan( int tacan );
    int                 vrati_tacan();
    bool                postavi_tezinu(int tezina);
    int                 vrati_tezinu();
    void                pravi_od_pitanja( Pitanje p ){
        oduzmi_mem();
        pravi_od(p);
    }
    QString             operator[](int i);         //da sluzi samo za citanje
    virtual QByteArray  za_slanje();
    virtual int         pravi_od(QString &s);
    bool                operator==(const Pitanje &p);
    Pitanje&            operator=(const Pitanje &p);
};


#endif // PITANJE_H
