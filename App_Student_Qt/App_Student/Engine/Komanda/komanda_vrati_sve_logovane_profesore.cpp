#include "komanda_vrati_sve_logovane_profesore.h"

komanda_vrati_sve_logovane_profesore::komanda_vrati_sve_logovane_profesore()
{
    Tip=KOMANDA_VRATI_SVE_LOGOVANE_PROFESORE;
}

komanda_vrati_sve_logovane_profesore::~komanda_vrati_sve_logovane_profesore()
{

}

QByteArray  komanda_vrati_sve_logovane_profesore::za_slanje(){
    // TIP
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int komanda_vrati_sve_logovane_profesore::praviOd(QString &s){

    // TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_VRATI_SVE_LOGOVANE_PROFESORE )
        return 0;

    return iterator;
}
