#include "komandaupit_sve_za_profesora.h"

KomandaUpit_Sve_za_Profesora::KomandaUpit_Sve_za_Profesora()
{
    Tip=KOMANDA_UPIT_PROFESOR_SVE;
    ID_prof=-1;
}

Odgovor_na_Komandu* KomandaUpit_Sve_za_Profesora::izvrsiNadBazom(QSqlDatabase &baza){
    //OBAVEZNO POGLEDATI KAKO RADI FJA
    //ZBOG SVIH REFERENCI I OSTAIH GLUPOSTI DA NESTO TU NE PRAVI PROVLEM :)
    komandaupit_predmet komanda_predmeti;
    komanda_predmeti.promeniIdProfesora(ID_prof);
    Odgovor_Predmeti *odg_predmeti=qobject_cast<Odgovor_Predmeti*>(
                komanda_predmeti.izvrsiNadBazom(baza));
    QList<Predmet> predmeti=odg_predmeti->vratiListu();
    delete odg_predmeti;
    //sada ovde imam listu predmeta
    //treba je napuniti oblastima

    Odgovor_Oblasti *odg_oblasti;
    komandaupit_oblast komanda_oblast;
    for( int i=0; i<predmeti.count(); i++){
        komanda_oblast.promeniIdProfesora(ID_prof);
        komanda_oblast.promeniIdPredmeta(predmeti[i].vrati_ID());
        odg_oblasti=qobject_cast<Odgovor_Oblasti*>( komanda_oblast.izvrsiNadBazom(baza) );
        QList<Oblast> oblasti_predmeta=odg_oblasti->vratiListu();
        delete odg_oblasti;
        odg_oblasti=NULL;
        predmeti[i].nadovezi_listu_oblasti(oblasti_predmeta);
    }
    //sada imamo listu predmeta koji sadrze oblasti
    //treba tim oblastima dodati kvizove i pitanja
    //prvo cemo svima da dodamo pitanja
    Odgovor_Pitanja *odg_pitanja;
    Odgovor_Kvizovi *odg_kvizovi;
    komandaupit_pitanje komanda_pitanje;
    komandaupit_kviz    komanda_kviz;
    for( int i=0; i<predmeti.count(); i++ ){
        int n=predmeti[i].broj_oblasti();
        for( int j=0; j<n; j++ ){
            //prolazimo kroz oblasti
            komanda_pitanje.promeniIdProfesora(ID_prof);
            komanda_pitanje.promeniIdPredmeta(predmeti[i].vrati_ID());
            komanda_pitanje.promeniIdOblasti(predmeti[i].vrati_oblast_na(j).vrati_ID());
            odg_pitanja=qobject_cast<Odgovor_Pitanja*>( komanda_pitanje.izvrsiNadBazom(baza) );
            QList<Pitanje> lista_pitanja=odg_pitanja->vratiListu();
            delete odg_pitanja;
            odg_pitanja=NULL;
            predmeti[i].vrati_oblast_na(j).nadovezi_listu_pitanja(lista_pitanja);
            //nadovezana pitanja
            //sada nadovezati kvizove
            komanda_kviz.promeniIdProfesora(ID_prof);
            komanda_kviz.promeniIdPredmeta(predmeti[i].vrati_ID());
            komanda_kviz.promeniIdOblasti(predmeti[i].vrati_oblast_na(j).vrati_ID());
            odg_kvizovi=qobject_cast<Odgovor_Kvizovi*>( komanda_kviz.izvrsiNadBazom(baza) );
            QList<Kviz> kvizovi=odg_kvizovi->vratiListu();
            delete odg_kvizovi;
            komandaupit_pitanja_za_kviz komanda_pitanja_kviz;
            for( int k=0; k<kvizovi.count(); k++ ){
                komanda_pitanja_kviz.postavi_ID_KVIZA(kvizovi[k].vrati_ID());
                odg_pitanja=qobject_cast<Odgovor_Pitanja*>(
                            komanda_pitanja_kviz.izvrsiNadBazom(baza));
                QList<Pitanje> pitanja_za_kviz=odg_pitanja->vratiListu();
                delete odg_pitanja;
                kvizovi[k].dodaj_listu_pitanja(pitanja_za_kviz);//OPREZ
            }
            predmeti[i].vrati_oblast_na(j).nadovezi_listu_kvizova(kvizovi);//OPREZ
        }
    }
    Odgovor_Predmeti *odgovor=new Odgovor_Predmeti();
    odgovor->dodaj_listu_predmeta(predmeti);
    return odgovor;
}
QByteArray KomandaUpit_Sve_za_Profesora::za_slanje(){
    //TIP  || ID_PROF
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_prof)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int KomandaUpit_Sve_za_Profesora::praviOd(QString &izvorniQString){
    //TIP  || sifra_kviza
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(izvorniQString,iterator);
    if( pom.toInt()!=KOMANDA_UPIT_PROFESOR_SVE )
        return 0;

    pom=dekapsulacija(izvorniQString,iterator);
    ID_prof=pom.toInt();

    return iterator;
}
