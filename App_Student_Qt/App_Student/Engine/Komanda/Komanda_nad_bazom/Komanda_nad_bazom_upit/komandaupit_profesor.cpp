#include "komandaupit_profesor.h"

komandaupit_profesor::komandaupit_profesor()
{
    Tabela=PROFESOR;
    Tip=KOMANDA_UPIT_PROFESORI;
}

komandaupit_profesor::~komandaupit_profesor()
{

}
QString komandaupit_profesor::toQuery()
{
    /*SELECT *
    FROM table
    WHERE [ conditions ]*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if( IdProfesora!=-1 ){
        if(values!="")
            values=values + " AND "+ QString(TABELA_PROFESOR_ID) +"="+enkapsulirajZaBazu(IdProfesora);
        else
            values=values + QString(TABELA_PROFESOR_ID)+"=" +enkapsulirajZaBazu(IdProfesora);
    }
    if(Ime!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_PROFESOR_IME) +"="+enkapsulirajZaBazu(Ime);
        else
            values=values + QString(TABELA_PROFESOR_IME)+"=" +enkapsulirajZaBazu(Ime);
    }
    if(Prezime!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_PROFESOR_PREZIME)+"=" +enkapsulirajZaBazu(Prezime);
        else
            values=values + QString(TABELA_PROFESOR_PREZIME)+"=" +enkapsulirajZaBazu(Prezime);
    }
    if(Email!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_PROFESOR_E_MAIL)+"=" +enkapsulirajZaBazu(Email);
        else
            values=values + QString(TABELA_PROFESOR_E_MAIL)+"=" +enkapsulirajZaBazu(Email);
    }
    if(Username!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_PROFESOR_USERNAME)+"=" +enkapsulirajZaBazu(Username);
        else
            values=values + QString(TABELA_PROFESOR_USERNAME)+"="+enkapsulirajZaBazu(Username);
    }
    if(Password!="")
    {
        if(values!="")
            values=values +" AND "+ QString(TABELA_PROFESOR_PASSWORD) +"="+enkapsulirajZaBazu(Password);
        else
            values=values + QString(TABELA_PROFESOR_PASSWORD)+"=" +enkapsulirajZaBazu(Password);
    }

    QString temp;
    if( values!="" )
        temp="SELECT * FROM " + QString(TABELA_PROFESOR) +" WHERE "+values+";";
    else
        temp="SELECT * FROM " + QString(TABELA_PROFESOR) +";";

    std::cout<<temp.toStdString()<<std::endl;
    return temp;
}
QByteArray komandaupit_profesor::za_slanje()
{
    // Tip || table || IdProfesora || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(Ime));
    povratniString.append(enkapsuliraj(Prezime));
    povratniString.append(enkapsuliraj(Email));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandaupit_profesor::praviOd(QString &izvorniQString)
{
    // Tip || table || IdProfesora || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_UPIT_PROFESORI)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PROFESOR)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPrezime(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniEmail(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}
