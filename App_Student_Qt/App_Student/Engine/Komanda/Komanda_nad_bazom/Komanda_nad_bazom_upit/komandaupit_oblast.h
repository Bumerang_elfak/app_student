#ifndef KOMANDAUPIT_OBLAST_H
#define KOMANDAUPIT_OBLAST_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandaupit_oblast : public KomandaUpit
{
    Q_OBJECT
public:
    komandaupit_oblast();
    ~komandaupit_oblast();

    void promeniIdOblasti(int novo) {IdOblasti=novo;}
    void promeniImeOblasti(QString novo) {ImeOblasti=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}
    void promeniIdPredmeta(int novo){IdPredmeta=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    Tipovi Tabela;
    int IdOblasti=-1;
    QString ImeOblasti="";
    int IdProfesora=-1;
    int IdPredmeta=-1;

};

#endif // KOMANDAUPIT_OBLAST_H
