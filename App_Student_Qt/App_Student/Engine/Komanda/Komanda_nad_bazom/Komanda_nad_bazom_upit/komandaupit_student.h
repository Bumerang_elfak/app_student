#ifndef KOMANDAUPIT_STUDENT_H
#define KOMANDAUPIT_STUDENT_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandaupit_student : public KomandaUpit
{
    Q_OBJECT
public:
    komandaupit_student();
    ~komandaupit_student();

    void promeniIdStudenta(int novo) {IdStudenta=novo;}
    void promeniUsername(QString novo){Username=novo;}
    void promeniPassword(QString novo){Password=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    Tipovi Tabela;
    int IdStudenta=-1;
    QString Username="";
    QString Password="";
};

#endif // KOMANDAUPIT_STUDENT_H
