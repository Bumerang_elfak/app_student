#ifndef KOMANDAUPIT_SVE_ZA_PROFESORA_H
#define KOMANDAUPIT_SVE_ZA_PROFESORA_H

#include <QObject>
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_oblast.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_predmet.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanja_za_kviz.h"
#include "Engine/fje_za_kreiranje.h"

class KomandaUpit_Sve_za_Profesora : public KomandaUpit
{
    Q_OBJECT

    int ID_prof;

public:
    KomandaUpit_Sve_za_Profesora();
    virtual Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza);

    void postavi_id_prof( int ID ){
        ID_prof=ID;
    }
    int  vrati_id_prof(){
        return ID_prof;
    }

    virtual QString toQuery(){
        return "";
    }
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);

};

#endif // KOMANDAUPIT_SVE_ZA_PROFESORA_H
