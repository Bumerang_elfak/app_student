#ifndef KOMANDAUPIT_KVIZOVI_ZA_PITANJA_H
#define KOMANDAUPIT_KVIZOVI_ZA_PITANJA_H

#include <QObject>
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"

class komandaupit_kvizovi_za_pitanja : public KomandaUpit
{
    Q_OBJECT
    int id_pitanja;

public:

    void postavi_id_pitanja( int id ){
        id_pitanja=id;
    }
    int vrati_id_pitanja(){
        return id_pitanja;
    }

    komandaupit_kvizovi_za_pitanja();

    virtual QString toQuery(){
        return  QString("SELECT * FROM ")+
                QString(TABELA_KVIZOVI_ZA_PITANJE)+
                QString(" WHERE ")+
                QString(TABELA_KVIZOVI_ZA_PITANJE_ID_PITANJA)+
                QString(" = ")+
                QString::number(id_pitanja)+
                QString(";");

    }

    virtual QByteArray za_slanje(){
        // TIP || id_pitanja
        using namespace Fje_za_Koriscenje;

        QString pom;
        pom.append(enkapsuliraj(QString::number(Tip)));
        pom.append(enkapsuliraj(QString::number(id_pitanja)));

        QByteArray ba;
        ba.append(pom);
        return ba;
    }
    virtual int praviOd(QString &izvorniQString){
        // TIP || id_pitanja
        using namespace Fje_za_Koriscenje;

        int iterator=0;
        QString pom;
        pom=dekapsulacija(izvorniQString,iterator);
        if( pom.toInt()!=KOMANDA_UPIT_KVIZOVI_ZA_PITANJE )
            return 0;

        pom=dekapsulacija(izvorniQString,iterator);
        id_pitanja=pom.toInt();
        return 1;
    }

};

#endif // KOMANDAUPIT_KVIZOVI_ZA_PITANJA_H
