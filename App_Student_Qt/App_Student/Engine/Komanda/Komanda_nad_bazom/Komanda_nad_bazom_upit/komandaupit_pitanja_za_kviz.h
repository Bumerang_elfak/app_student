#ifndef KOMANDAUPIT_PITANJA_ZA_KVIZ_H
#define KOMANDAUPIT_PITANJA_ZA_KVIZ_H

#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/fje_za_koriscenje.h"

class komandaupit_pitanja_za_kviz : public KomandaUpit
{
public:
    komandaupit_pitanja_za_kviz();

    void postavi_ID_KVIZA(int id){
        ID_KVIZA=id;
    }
    int vrati_ID_KVIZA(){
        return ID_KVIZA;
    }

    virtual QString toQuery(){
        // SELECT *
        // FROM TABELA_PITANJA_ZA_KVIZ
        // Where TABELA_PITANJA_ZA_KVIZ_ID = ID_KVIZA
        return  QString("SELECT * FROM ")+
                QString(TABELA_PITANJA_ZA_KVIZ)+
                QString(" WHERE ")+
                QString(TABELA_PITANJA_ZA_KVIZ_ID_KVIZA)+
                QString(" = ")+
                QString::number(ID_KVIZA)+
                QString(";");
    }

    virtual QByteArray za_slanje(){

        // TIP || ID_KVIZA
        using namespace Fje_za_Koriscenje;

        QString pom;
        pom.append(enkapsuliraj(QString::number(Tip)));
        pom.append(enkapsuliraj(QString::number(ID_KVIZA)));

        QByteArray ba;
        ba.append(pom);
        return ba;
    }
    virtual int praviOd(QString &izvorniQString){

        // TIP || ID_KVIZA
        using namespace Fje_za_Koriscenje;

        int iterator=0;
        QString pom;
        pom=dekapsulacija(izvorniQString,iterator);
        if( pom.toInt()!=KOMANDA_UPIT_PITANJA_ZA_KVIZ )
            return 0;

        pom=dekapsulacija(izvorniQString,iterator);
        ID_KVIZA=pom.toInt();
        return 1;
    }

private:
    int ID_KVIZA=-1;
};

#endif // KOMANDAUPIT_PITANJA_ZA_KVIZ_H
