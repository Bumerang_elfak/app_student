#ifndef KOMANDAUPIT_PREDMET_H
#define KOMANDAUPIT_PREDMET_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandaupit_predmet : public KomandaUpit
{
    Q_OBJECT
public:
    komandaupit_predmet();
    ~komandaupit_predmet();

    void promeniIdPredmeta(int novo) {IdPredmeta=novo;}
    void promeniImePredmeta(QString novo) {ImePredmeta=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    Tipovi Tabela;
    int IdPredmeta=-1;
    QString ImePredmeta="";
    int IdProfesora=-1;
};

#endif // KOMANDAUPIT_PREDMET_H
