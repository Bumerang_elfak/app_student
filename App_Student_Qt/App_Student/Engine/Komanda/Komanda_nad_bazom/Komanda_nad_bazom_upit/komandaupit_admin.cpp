#include "komandaupit_admin.h"

komandaupit_admin::komandaupit_admin()
{
    Tabela=ADMIN;
    Tip=KOMANDA_UPIT_ADMINI;
}

komandaupit_admin::~komandaupit_admin()
{

}

QString komandaupit_admin::toQuery()
{
    /*SELECT *
    FROM table
    WHERE [ conditions ]*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(Ime!=NULL && Ime!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_ADMIN_IME)+"=" +enkapsulirajZaBazu(Ime);
        else
            values=values + QString(TABELA_ADMIN_IME)+"=" +enkapsulirajZaBazu(Ime);
    }
    if(Prezime!=NULL && Prezime!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_ADMIN_PREZIME)+"=" +enkapsulirajZaBazu(Prezime);
        else
            values=values + QString(TABELA_ADMIN_PREZIME)+"=" +enkapsulirajZaBazu(Prezime);
    }
    if(Email!=NULL && Email!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_ADMIN_E_MAIL)+"=" +enkapsulirajZaBazu(Email);
        else
            values=values + QString(TABELA_ADMIN_E_MAIL)+"=" +enkapsulirajZaBazu(Email);
    }
    if(Username!=NULL && Username!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_ADMIN_USERNAME)+"=" +enkapsulirajZaBazu(Username);
        else
            values=values + QString(TABELA_ADMIN_USERNAME)+"=" +enkapsulirajZaBazu(Username);
    }
    if(Password!=NULL && Password!="")
    {
        if(values!="")
            values=values + " AND "+ QString(TABELA_ADMIN_PASSWORD)+"=" +enkapsulirajZaBazu(Password);
        else
            values=values + QString(TABELA_ADMIN_PASSWORD)+"=" +enkapsulirajZaBazu(Password);
    }

    QString temp;
    if( values!="" )
        temp="SELECT * FROM " + QString(TABELA_ADMIN) +" WHERE "+values+";";
    else
        temp="SELECT * FROM " + QString(TABELA_ADMIN)+";";

    std::cout<<temp.toStdString()<<std::endl;
    return temp;
}

QByteArray komandaupit_admin::za_slanje()
{
    // Tip || table || IdAdmina || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdAdmina)));
    povratniString.append(enkapsuliraj(Ime));
    povratniString.append(enkapsuliraj(Prezime));
    povratniString.append(enkapsuliraj(Email));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandaupit_admin::praviOd(QString &izvorniQString)
{
    // Tip || table || IdAdmina || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_UPIT_ADMINI)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=ADMIN)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdAdmina(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPrezime(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniEmail(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}
