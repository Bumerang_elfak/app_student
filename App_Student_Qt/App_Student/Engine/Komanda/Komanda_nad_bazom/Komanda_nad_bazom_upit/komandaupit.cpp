#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"



KomandaUpit::KomandaUpit()
{
    Tip=KOMANDA_UPIT;
}

KomandaUpit::~KomandaUpit()
{

}


//ZAPAMTITI DA SE RADI PO PRINCIPU LAZY LOAD!!!
//STO ZNACI DA SE U APLIKACIJI MORAJU DRZATI REF MA OBJ KOJI POSTOJE
//
//POGLEDATI PAZLJIVO STA SVAKA FJA VRACA TO JEST OBJEKAT KOJI VRACA!!!
//POGLEDATI PAZLJIVO IZ CEGA SE TAJ OBJEKAT SASTOJI
//KONSULTOVATI SE OKO POVRATNE VREDNOSTI AKO IMA POTREBE
//
//KOD PITANJA JE MNOGO BITAN REDOSLED ODGOVORA JER MI TACAN ODG
//PAMTIMO KAO BR ODGOVORA NA PITANJE U NIZU!!! OPREZNO SA TIM
//
Odgovor_na_Komandu *KomandaUpit::izvrsiNadBazom(QSqlDatabase &baza)
{
    QSqlQuery query(baza);
    QString upit=toQuery();
    Odgovor_na_Komandu *Odgovor=NULL;

    if(upit!="" && query.exec(upit)){   //provera dobrog izvrsenja upita
        //lepo izvrseno, odedjivanje tipa komande koja se izvrsila

        switch(Tip){
        case KOMANDA_UPIT_ADMINI:{//trenutno NISTA!!!

            break;
        }
        case KOMANDA_UPIT_KVIZOVI:{
            Odgovor_Kvizovi *ok=new Odgovor_Kvizovi();
            ok->postavi_adresu_povratka(adresa_povratka);
            while( query.next() ){
                Kviz k;
                k.postavi_ID(query.value(TABELA_INDEX_KVIZ_ID).toInt());
                k.postavi_ime(query.value(TABELA_INDEX_KVIZ_IME).toString());
                k.postavi_tezinu(query.value(TABELA_INDEX_KVIZ_TEZINA).toInt());
                k.postavi_ID_Profesora(query.value(TABELA_INDEX_KVIZ_ID_PROFESOR).toInt());
                k.postavi_ID_Predmeta(query.value(TABELA_INDEX_KVIZ_ID_PREDMET).toInt());
                k.postavi_ID_Oblasti(query.value(TABELA_INDEX_KVIZ_ID_OBLASTI).toInt());
                // moze se izvuci jos podataka ali msm da nije potrebno!!!
                // na pr. br.pitanja je dobro izvuci, ali to se dobija
                // tako sto se izracuna sa koliko je pitanja taj kviz
                // u relaciji pa se odatle izvlaci
                //std::cout<<k.za_slanje().toStdString()<<std::endl;
                ok->dodaj_kviz(k);
            }
            Odgovor=ok;
            //std::cout<<ok->za_slanje().toStdString()<<std::endl;
            break;
        }
        case KOMANDA_UPIT_OBLASTI:{
            Odgovor_Oblasti *oo=new Odgovor_Oblasti();
            oo->postavi_adresu_povratka(adresa_povratka);
            while(query.next()){
                Oblast o;
                o.postavi_ID(query.value(TABELA_INDEX_OBLAST_ID).toInt());
                o.postavi_ime(query.value(TABELA_INDEX_OBLAST_IME).toString());
                o.postavi_ID_Profesora(query.value(TABELA_INDEX_OBLAST_ID_PROFESORA).toInt());
                o.postavi_ID_Predmeta(query.value(TABELA_INDEX_OBLAST_ID_PREDMETA).toInt());
                oo->dodaj_oblast(o);
            }
            Odgovor=oo;
            break;
        }
        case KOMANDA_UPIT_PITANJA:{
            Odgovor_Pitanja *op=new Odgovor_Pitanja;
            op->postavi_adresu_povratka(adresa_povratka);
            while(query.next()){
                Pitanje p;
                p.postavi_ID(query.value(TABELA_INDEX_PITANJE_ID).toInt());
                p.postavi_ime(query.value(TABELA_INDEX_PITANJE_TEKST).toString());
                p.postavi_tezinu(query.value(TABELA_INDEX_PITANJE_TEZINA).toInt());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG1).toString());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG2).toString());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG3).toString());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG4).toString());
                p.postavi_tacan(query.value(TABELA_INDEX_PITANJE_TACAN).toInt());
                p.postavi_ID_Profesora(query.value(TABELA_INDEX_PITANJE_ID_PROFESORA).toInt());
                p.postavi_ID_Predmeta(query.value(TABELA_INDEX_PITANJE_ID_PREDMETA).toInt());
                p.postavi_ID_Oblasti(query.value(TABELA_INDEX_PITANJE_ID_OBLASTI).toInt());
                op->dodaj_pitanje(p);
            }
            Odgovor=op;
            break;
        }
        case KOMANDA_UPIT_PITANJA_ZA_KVIZ:{
            Odgovor_Pitanja *op=new Odgovor_Pitanja;
            op->postavi_adresu_povratka(adresa_povratka);
            while(query.next()){
                Pitanje p;
                p.postavi_ID(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ID_PITANJA).toInt());
                p.postavi_ime(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_TEKST).toString());
                p.postavi_tezinu(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_TEZINA).toInt());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ODG1).toString());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ODG2).toString());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ODG3).toString());
                p.dodaj_odg(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ODG4).toString());
                p.postavi_tacan(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_TACAN).toInt());
                p.postavi_ID_Profesora(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ID_PROFESORA).toInt());
                p.postavi_ID_Predmeta(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ID_PREDMETA).toInt());
                p.postavi_ID_Oblasti(query.value(TABELA_INDEX_PITANJA_ZA_KVIZ_ID_OBLASTI).toInt());
                op->dodaj_pitanje(p);
            }
            Odgovor=op;
            break;
        }
        case KOMANDA_UPIT_PREDMETI:{
            Odgovor_Predmeti *op=new Odgovor_Predmeti();
            op->postavi_adresu_povratka(adresa_povratka);
            while(query.next()){
                Predmet p;
                p.postavi_ID(query.value(TABELA_INDEX_PREDMET_ID).toInt());
                p.postavi_ime(query.value(TABELA_INDEX_PREDMET_IME).toString());
                p.postavi_ID_Profesora(query.value(TABELA_INDEX_PREDMET_ID_PROFESORA).toInt());
                op->dodaj_predmet(p);
            }
            Odgovor=op;//is FAGGET
            break;
        }
        case KOMANDA_UPIT_PROFESORI:{
            Odgovor_Profesori *op=new Odgovor_Profesori();
            op->postavi_adresu_povratka(adresa_povratka);
            while( query.next() ){
                Profesor p;
                p.promeniId(query.value(TABELA_INDEX_PROFESOR_ID).toInt());
                p.promeniIme(query.value(TABELA_INDEX_PROFESOR_IME).toString());
                p.promeniPrezime(query.value(TABELA_INDEX_PROFESOR_PREZIME).toString());
                p.promeniUsername(query.value(TABELA_INDEX_PROFESOR_USERNAME).toString());
                p.promeniPassword(query.value(TABELA_INDEX_PROFESOR_PASSWORD).toString());
                p.promeniEmail(query.value(TABELA_INDEX_PROFESOR_E_MAIL).toString());
                op->dodaj_profesor(p);
            }
            Odgovor=op;
            break;
        }
        case KOMANDA_UPIT_PROFESOR_BEZ_PREDMETA:{
            Odgovor_Profesori *op=new Odgovor_Profesori();
            op->postavi_adresu_povratka(adresa_povratka);
            while( query.next() ){
                Profesor p;
                p.promeniId(query.value(TABELA_INDEX_PROFESOR_BEZ_PREDMETA_ID).toInt());
                p.promeniIme(query.value(TABELA_INDEX_PROFESOR_BEZ_PREDMETA_IME).toString());
                p.promeniPrezime(query.value(TABELA_INDEX_PROFESOR_BEZ_PREDMETA_PREZIME).toString());
                p.promeniUsername(query.value(TABELA_INDEX_PROFESOR_BEZ_PREDMETA_USERNAME).toString());
                p.promeniPassword(query.value(TABELA_INDEX_PROFESOR_BEZ_PREDMETA_PASSWORD).toString());
                p.promeniEmail(query.value(TABELA_INDEX_PROFESOR_BEZ_PREDMETA_E_MAIL).toString());
                op->dodaj_profesor(p);
            }
            Odgovor=op;
            break;
        }
        case KOMANDA_UPIT_STUDENTI:{//za sada ne koristiti!!!

            break;
        }
        case KOMANDA_UPIT_KVIZOVI_ZA_PITANJE:{
            Odgovor_Kvizovi *ok=new Odgovor_Kvizovi();
            ok->postavi_adresu_povratka(adresa_povratka);
            while( query.next() ){
                Kviz k;
                k.postavi_ID(query.value(TABELA_INDEX_KVIZOVI_ZA_PITANJE_ID_KVIZA).toInt());
                k.postavi_ime(query.value(TABELA_INDEX_KVIZOVI_ZA_PITANJE_IME_KVIZA).toString());
                k.postavi_tezinu(query.value(TABELA_INDEX_KVIZOVI_ZA_PITANJE_TEZINA_KVIZA).toInt());
                k.postavi_ID_Profesora(query.value(TABELA_INDEX_KVIZOVI_ZA_PITANJE_ID_PROFESORA).toInt());
                k.postavi_ID_Predmeta(query.value(TABELA_INDEX_KVIZOVI_ZA_PITANJE_ID_PREDMETA).toInt());
                k.postavi_ID_Oblasti(query.value(TABELA_INDEX_KVIZOVI_ZA_PITANJE_ID_OBLASTI).toInt());
                // moze se izvuci jos podataka ali msm da nije potrebno!!!
                // na pr. br.pitanja je dobro izvuci, ali to se dobija
                // tako sto se izracuna sa koliko je pitanja taj kviz
                // u relaciji pa se odatle izvlaci
                //std::cout<<k.za_slanje().toStdString()<<std::endl;
                ok->dodaj_kviz(k);
            }
            Odgovor=ok;
            break;
        }
        default:{//ako nije nista od gore onda greska
            Odgovor=new Error();
            Odgovor->postavi_adresu_povratka(adresa_povratka);
            qobject_cast<Error*>(Odgovor)->postavi_greska("Greska prilikom selekcije podataka u bazu");
        }
        }
    }
    else{//greska prilikom upita
        Odgovor=new Error();
        Odgovor->postavi_adresu_povratka(adresa_povratka);
        qobject_cast<Error*>(Odgovor)->postavi_greska("Greska prilikom selekcije podataka u bazu");
    }
    return Odgovor;
}
