#ifndef KOMANDAUPIT_PITANJE_H
#define KOMANDAUPIT_PITANJE_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandaupit_pitanje : public KomandaUpit
{
    Q_OBJECT
public:
    komandaupit_pitanje();
    ~komandaupit_pitanje();

        void promeniIdPitanja(int novo) {IdPitanja=novo;}
    void promeniTekstPitanja(QString novo) {TekstPitanja=novo;}
    void promeniOdgovor1(QString novo) {Odgovor1=novo;}
    void promeniOdgovor2(QString novo) {Odgovor2=novo;}
    void promeniOdgovor3(QString novo) {Odgovor3=novo;}
    void promeniOdgovor4(QString novo) {Odgovor4=novo;}
    void promeniTacanOdgovor(int novo){TacanOdgovor=novo;}
    void promeniTezinuPitanja(int novo){TezinaPitanja=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}
    void promeniIdPredmeta(int novo){IdPredmeta=novo;}
    void promeniIdOblasti(int novo){IdOblasti=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    Tipovi Tabela;
    int IdPitanja=-1;
    QString TekstPitanja="";
    QString Odgovor1="";
    QString Odgovor2="";
    QString Odgovor3="";
    QString Odgovor4="";
    int TacanOdgovor=-1;
    int TezinaPitanja=-1;
    int IdProfesora=-1;
    int IdPredmeta=-1;
    int IdOblasti=-1;

    // Tip || Tabela || TekstPitanja || Odgovor1 || Odgovor2 || Odgovor3 || Odgovor4
    // TacanOdgovor || TezinaPitanja || IdProfesora || IdPredmeta || IdOblasti
};

#endif // KOMANDAUPIT_PITANJE_H
