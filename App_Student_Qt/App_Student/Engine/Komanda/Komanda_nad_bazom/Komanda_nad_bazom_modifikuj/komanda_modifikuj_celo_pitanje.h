#ifndef KOMANDA_MODIFIKUJ_CELO_PITANJE_H
#define KOMANDA_MODIFIKUJ_CELO_PITANJE_H

#include <QObject>
#include "Engine/PPOK/pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kvizovi_za_pitanja.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_pitanja_za_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_kviz.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_pitanja.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_kvizovi.h"

class Komanda_Modifikuj_celo_Pitanje : public KomandaModifikuj
{
    Q_OBJECT

    Pitanje pitanje_za_modifikaciju;

public:
    Komanda_Modifikuj_celo_Pitanje();

    void    postavi_pitanje( Pitanje p ){
        pitanje_za_modifikaciju.pravi_od_pitanja(p);
    }

    Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza);
    virtual QString toQuery(){//PRAZNO JER NEMA QUERY SINE
        return "";
    }

    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);

};

#endif // KOMANDA_MODIFIKUJ_CELO_PITANJE_H
