#include "komandamodifikuj_pitanje.h"

komandamodifikuj_pitanje::komandamodifikuj_pitanje()
{
    Tabela=PITANJE;
}
komandamodifikuj_pitanje::~komandamodifikuj_pitanje()
{

}

QString komandamodifikuj_pitanje::toQuery()
{
    /*UPDATE table_name
    SET column1=value1,column2=value2,...
    WHERE TABLE_ID=ID;*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(TekstPitanja!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_TEKST) +"="+enkapsulirajZaBazu(TekstPitanja);
        else
            values=values + QString(TABELA_PITANJE_TEKST) +"="+enkapsulirajZaBazu(TekstPitanja);
    }
    if(Odgovor1!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_ODG1)+"=" +enkapsulirajZaBazu(Odgovor1);
        else
            values=values + QString(TABELA_PITANJE_ODG1)+"=" +enkapsulirajZaBazu(Odgovor1);
    }
    if(Odgovor2!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_ODG2)+"=" +enkapsulirajZaBazu(Odgovor2);
        else
            values=values + QString(TABELA_PITANJE_ODG2) +"="+enkapsulirajZaBazu(Odgovor2);
    }
    if(Odgovor3!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_ODG3)+"=" +enkapsulirajZaBazu(Odgovor3);
        else
            values=values + QString(TABELA_PITANJE_ODG3) +"="+enkapsulirajZaBazu(Odgovor3);
    }
    if(Odgovor4!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_ODG4) +"="+enkapsulirajZaBazu(Odgovor4);
        else
            values=values + QString(TABELA_PITANJE_ODG4) +"="+enkapsulirajZaBazu(Odgovor4);
    }
    if(TacanOdgovor!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_TACAN)+"=" +enkapsulirajZaBazu(TacanOdgovor);
        else
            values=values + QString(TABELA_PITANJE_TACAN) +"="+enkapsulirajZaBazu(TacanOdgovor);
    }
    if(TezinaPitanja!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_TEZINA) +"="+enkapsulirajZaBazu(TezinaPitanja);
        else
            values=values + QString(TABELA_PITANJE_TEZINA)+"=" +enkapsulirajZaBazu(TezinaPitanja);
    }
    if(IdProfesora!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_ID_PROFESORA)+"=" +enkapsulirajZaBazu(IdProfesora);
        else
            values=values + QString(TABELA_PITANJE_ID_PROFESORA)+"=" +enkapsulirajZaBazu(IdProfesora);
    }
    if(IdPredmeta!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_ID_PREDMETA)+"=" +enkapsulirajZaBazu(IdPredmeta);
        else
            values=values + QString(TABELA_PITANJE_ID_PREDMETA)+"=" +enkapsulirajZaBazu(IdPredmeta);
    }
    if(IdOblasti!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_PITANJE_ID_OBLASTI)+"=" +enkapsulirajZaBazu(IdOblasti);
        else
            values=values + QString(TABELA_PITANJE_ID_OBLASTI)+"=" +enkapsulirajZaBazu(IdOblasti);
    }

    QString temp="UPDATE " + QString(TABELA_PITANJE) +" SET "+values+ " WHERE "+
            QString(TABELA_PITANJE_ID)+"="+QString::number(IdPitanja)+";";

    return temp;
}

QByteArray komandamodifikuj_pitanje::za_slanje()
{
    // Tip || Tabela || IdPitanja || TekstPitanja || Odgovor1 || Odgovor2 || Odgovor3 || Odgovor4
    // TacanOdgovor || TezinaPitanja || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdPitanja)));
    povratniString.append(enkapsuliraj(TekstPitanja));
    povratniString.append(enkapsuliraj(Odgovor1));
    povratniString.append(enkapsuliraj(Odgovor2));
    povratniString.append(enkapsuliraj(Odgovor3));
    povratniString.append(enkapsuliraj(Odgovor4));
    povratniString.append(enkapsuliraj(QString::number(TacanOdgovor)));
    povratniString.append(enkapsuliraj(QString::number(TezinaPitanja)));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));
    povratniString.append(enkapsuliraj(QString::number(IdOblasti)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandamodifikuj_pitanje::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdPitanja || TekstPitanja || Odgovor1 || Odgovor2 || Odgovor3 || Odgovor4
    // TacanOdgovor || TezinaPitanja || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_MODIFIKUJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PITANJE)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPitanja(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniTekstPitanja(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor1(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor2(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor3(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniOdgovor4(pomocna);

    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniTacanOdgovor(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniTezinuPitanja(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdOblasti(pomocna.toInt());
    return iterator;
}
