#include "komandamodifikuj_oblast.h"

komandamodifikuj_oblast::komandamodifikuj_oblast()
{
    Tabela=OBLAST;
}

komandamodifikuj_oblast::~komandamodifikuj_oblast()
{

}

QString komandamodifikuj_oblast::toQuery()
{
    /*UPDATE table_name
    SET column1=value1,column2=value2,...
    WHERE TABLE_ID=ID;*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(ImeOblasti!=NULL && ImeOblasti!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_OBLAST_IME)+"=" +enkapsulirajZaBazu(ImeOblasti);
        else
            values=values + QString(TABELA_OBLAST_IME)+"=" +enkapsulirajZaBazu(ImeOblasti);
    }
    if(IdProfesora!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_OBLAST_ID_PROFESORA) +"="+enkapsulirajZaBazu(IdProfesora);
        else
            values=values + QString(TABELA_OBLAST_ID_PROFESORA)+"=" +enkapsulirajZaBazu(IdProfesora);
    }
    if(IdPredmeta!=-1)
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_OBLAST_ID_PREDMETA)+"=" +enkapsulirajZaBazu(IdPredmeta);
        else
            values=values + QString(TABELA_OBLAST_ID_PREDMETA)+"=" +enkapsulirajZaBazu(IdPredmeta);
    }

    QString temp="UPDATE " + QString(TABELA_OBLAST) +" SET "+values+ " WHERE "+
            QString(TABELA_OBLAST_ID)+"="+QString::number(IdOblasti)+";";

    return temp;
}
QByteArray komandamodifikuj_oblast::za_slanje()
{
    // Tip || Tabela || IdOblasti || ImeOblasti || IdProfesora || IdPredmeta
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdOblasti)));
    povratniString.append(enkapsuliraj(ImeOblasti));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandamodifikuj_oblast::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdOblasti || ImeOblasti || IdProfesora || IdPredmeta
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_MODIFIKUJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=OBLAST)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdOblasti(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniImeOblasti(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    return iterator;
}
