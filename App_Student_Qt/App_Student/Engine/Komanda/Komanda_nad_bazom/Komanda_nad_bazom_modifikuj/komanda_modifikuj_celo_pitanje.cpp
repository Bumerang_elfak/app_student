#include "komanda_modifikuj_celo_pitanje.h"

Komanda_Modifikuj_celo_Pitanje::Komanda_Modifikuj_celo_Pitanje(){
    Tip=KOMANDA_MODIFIKUJ_CELO_PITANJE;
}
Odgovor_na_Komandu* Komanda_Modifikuj_celo_Pitanje::izvrsiNadBazom(QSqlDatabase &baza){
    baza.transaction();
    komandaupit_pitanje kom;
    kom.promeniIdPitanja(pitanje_za_modifikaciju.vrati_ID());
    Odgovor_na_Komandu *odg=kom.izvrsiNadBazom(baza);
    if( odg==NULL || odg->vrati_tip()==ERROR ){
        baza.rollback();
        return odg;
    }
    Odgovor_Pitanja *odg_pitanje=qobject_cast<Odgovor_Pitanja*>(odg);

    if( odg_pitanje==NULL ){
        baza.rollback();
        return NULL;
    }
    int stara_tezina=odg_pitanje->vratiListu()[0].vrati_tezinu();//u listi moze biti samo jedno pitanje
    delete odg_pitanje;

    komandamodifikuj_pitanje kom_mod_pitanje;
    kom_mod_pitanje.promeniIdPitanja(pitanje_za_modifikaciju.vrati_ID());
    kom_mod_pitanje.promeniTezinuPitanja(pitanje_za_modifikaciju.vrati_tezinu());
    odg=kom_mod_pitanje.izvrsiNadBazom(baza);
    if( odg==NULL || odg->vrati_tip()==ERROR ){
        baza.rollback();
        return odg;
    }
    delete odg;

    //e sada pocinje SHOW xD ovde je odgovor modifikovan daj koji treba da bude
    //sada svim kvizovima koji poseduju to pitanje treba promeniti statistiku!!!
    komandaupit_kvizovi_za_pitanja kom_kv_za_pit;
    kom_kv_za_pit.postavi_id_pitanja(pitanje_za_modifikaciju.vrati_ID());
    odg=kom_kv_za_pit.izvrsiNadBazom(baza);
    Odgovor_Kvizovi *odg_kviz=qobject_cast<Odgovor_Kvizovi*>(odg);
    if( odg==NULL || odg->vrati_tip()==ERROR ){
        baza.rollback();
        return odg;
    }
    QList<Kviz> kvizovi_za_menjanje=odg_kviz->vratiListu();
    delete odg_kviz;
    for( int i=0; i<kvizovi_za_menjanje.count(); i++ ){
        komandaupit_kviz kom_ukviz;
        kom_ukviz.promeniIdKviza(kvizovi_za_menjanje[i].vrati_ID());
        odg=kom_ukviz.izvrsiNadBazom(baza);
        if( odg==NULL || odg->vrati_tip()==ERROR ){
            baza.rollback();
            return odg;
        }
        Odgovor_Kvizovi *odg_kviz_jedan=qobject_cast<Odgovor_Kvizovi*>(odg);
        int tezina_kviza=odg_kviz_jedan->vratiListu()[0].vrati_tezinu();
        //sada trebamo da izvucemo sva pitanja datog kviza da bi videli broj pitanja
        //ovo mozemo da odradimo i novom komandom tipa select count
        //ali za sada ce ovako da bude realizovano
        delete odg;
        komandaupit_pitanja_za_kviz kom_pitanja_tren_kviz;
        kom_pitanja_tren_kviz.postavi_ID_KVIZA(kvizovi_za_menjanje[i].vrati_ID());
        odg=kom_pitanja_tren_kviz.izvrsiNadBazom(baza);
        if( odg==NULL || odg->vrati_tip()==ERROR ){
            baza.rollback();
            return odg;
        }
        int broj_pitanja=qobject_cast<Odgovor_Pitanja*>(odg)->vratiListu().count();
        delete odg;
        tezina_kviza=tezina_kviza - ( stara_tezina/broj_pitanja )
                                  + ( pitanje_za_modifikaciju.vrati_tezinu()/broj_pitanja );


        komandamodifikuj_kviz kom_mkviz;
        kom_mkviz.promeniIdKviza(kvizovi_za_menjanje[i].vrati_ID());
        kom_mkviz.promenitezinu(tezina_kviza);
        odg=kom_mkviz.izvrsiNadBazom(baza);
        if( odg==NULL || odg->vrati_tip()==ERROR ){
            baza.rollback();
            return odg;
        }
        delete odg;
    }

    Odgovor_Modifikovan *odgovor_kraj=new Odgovor_Modifikovan();
    odgovor_kraj->postaviOK(true);
    return odgovor_kraj;
}
QByteArray Komanda_Modifikuj_celo_Pitanje::za_slanje(){
    // TIP || PITANJE
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(pitanje_za_modifikaciju.za_slanje()));

    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int Komanda_Modifikuj_celo_Pitanje::praviOd(QString &izvorniQString){
    // TIP || PITANJE
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_MODIFIKUJ_CELO_PITANJE)
        return 0;

    QString ba;
    ba.append(dekapsulacija(pomocna,iterator));
    pitanje_za_modifikaciju.pravi_od(ba);
    return iterator;
}
