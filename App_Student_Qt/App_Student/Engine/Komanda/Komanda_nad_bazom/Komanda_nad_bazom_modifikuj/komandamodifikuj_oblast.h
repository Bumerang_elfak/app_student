#ifndef KOMANDAMODIFIKUJ_OBLAST_H
#define KOMANDAMODIFIKUJ_OBLAST_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komandamodifikuj.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandamodifikuj_oblast : public KomandaModifikuj
{
    Q_OBJECT
public:
    komandamodifikuj_oblast();
    ~komandamodifikuj_oblast();

    void promeniIdOblasti(int novo) {IdOblasti=novo;}
    void promeniImeOblasti(QString novo) {ImeOblasti=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}
    void promeniIdPredmeta(int novo){IdPredmeta=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:
    Tipovi Tabela;
    int IdOblasti;
    QString ImeOblasti="";
    int IdProfesora=-1;
    int IdPredmeta=-1;

};

#endif // KOMANDAMODIFIKUJ_OBLAST_H
