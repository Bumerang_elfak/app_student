#include "komandamodifikuj_student.h"

komandamodifikuj_student::komandamodifikuj_student()
{
    Tabela=STUDENT;
}

komandamodifikuj_student::~komandamodifikuj_student()
{

}
QString komandamodifikuj_student::toQuery()
{
    /*UPDATE table_name
    SET column1=value1,column2=value2,...
    WHERE TABLE_ID=ID;*/
    using namespace Fje_za_Koriscenje;

    QString values="";

    if(Username!=NULL && Username!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_STUDENT_USERNAME)+"=" +enkapsulirajZaBazu(Username);
        else
            values=values + QString(TABELA_STUDENT_USERNAME)+"=" +enkapsulirajZaBazu(Username);
    }
    if(Password!=NULL && Password!="")
    {
        if(values!="")
            values=values + ", "+ QString(TABELA_STUDENT_PASSWORD) +"="+enkapsulirajZaBazu(Password);
        else
            values=values + QString(TABELA_STUDENT_PASSWORD)+"=" +enkapsulirajZaBazu(Password);
    }

    QString temp="UPDATE " + QString(TABELA_STUDENT) +" SET "+values+ " WHERE "+
            QString(TABELA_STUDENT_ID)+"="+QString::number(IdStudenta)+";";

    return temp;
}
QByteArray komandamodifikuj_student::za_slanje()
{
    // Tip || table || IdStudenta || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(QString::number(IdStudenta)));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandamodifikuj_student::praviOd(QString &izvorniQString)
{
    // Tip || table || IdStudenta || Username || Password
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_MODIFIKUJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=STUDENT)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdStudenta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}
