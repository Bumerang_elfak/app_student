#ifndef KOMANDAMODIFIKUJ_H
#define KOMANDAMODIFIKUJ_H
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>
#include <QObject>
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_modifikovan.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include <iostream>

class KomandaModifikuj : public KomandaNadBazom
{
    Q_OBJECT
public:
    KomandaModifikuj();
    virtual ~KomandaModifikuj();

    Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza);
    virtual QString toQuery()=0;

    virtual QByteArray za_slanje()=0;
    virtual int praviOd(QString &izvorniQString)=0;

    /*UPDATE table_name
SET column1=value1,column2=value2,...
WHERE some_column=some_value;*/
};

#endif // KOMANDAMODIFIKUJ_H
