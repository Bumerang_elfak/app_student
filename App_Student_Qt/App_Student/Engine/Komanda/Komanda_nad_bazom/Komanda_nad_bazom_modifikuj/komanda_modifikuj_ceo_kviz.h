#ifndef KOMANDA_MODIFIKUJ_CEO_KVIZ_H
#define KOMANDA_MODIFIKUJ_CEO_KVIZ_H

#include <QObject>
#include "Engine/PPOK/kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_modifikuj/komanda_modifikuj_celo_pitanje.h"

class Komanda_Modifikuj_Ceo_Kviz : public KomandaModifikuj
{
    Q_OBJECT
    Kviz kviz_za_modifikaciju;

public:
    Komanda_Modifikuj_Ceo_Kviz();
    void postavi_kviz_za_modifikaciju( Kviz k ){
        kviz_za_modifikaciju.pravi_od_kviza(k);
    }

    Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza);
    virtual QString toQuery(){//PRAZNO JER NEMA QUERY SINE
        return "";
    }

    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);

};

#endif // KOMANDA_MODIFIKUJ_CEO_KVIZ_H
