#include "komandadodaj_predmet.h"

komandadodaj_predmet::komandadodaj_predmet()
{
    Tabela=PREDMET;
}
komandadodaj_predmet::~komandadodaj_predmet()
{

}
QString komandadodaj_predmet::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_PREDMET)+" ("+QString(TABELA_PREDMET_IME)+", "+QString(TABELA_PREDMET_ID_PROFESORA);
    temp=temp+") VALUES("+enkapsulirajZaBazu(ImePredmeta)+", "+enkapsulirajZaBazu(IdProfesora)+");";

    return temp;
}

QByteArray komandadodaj_predmet::za_slanje()
{
    // Tip || Tabela || ImePredmeta || IdProfesora
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(ImePredmeta));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_predmet::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || IdPitanja || IdKviza
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PREDMET)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniImePredmeta(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    return iterator;
}
