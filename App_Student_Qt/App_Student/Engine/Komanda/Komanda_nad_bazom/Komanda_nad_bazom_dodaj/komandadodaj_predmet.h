#ifndef KOMANDADODAJ_PREDMET_H
#define KOMANDADODAJ_PREDMET_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandadodaj_predmet : public KomandaDodaj
{
    Q_OBJECT
public:
    komandadodaj_predmet();
    ~komandadodaj_predmet();

    void promeniImePredmeta(QString novo) {ImePredmeta=novo;}
    void promeniIdProfesora(int novo){IdProfesora=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:

    QString ImePredmeta="";
    int IdProfesora;
};

#endif // KOMANDADODAJ_PREDMET_H
