#ifndef KOMANDADODAJ_STUDENT_H
#define KOMANDADODAJ_STUDENT_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/fje_za_koriscenje.h"

class komandadodaj_student : public KomandaDodaj
{
    Q_OBJECT
public:
    komandadodaj_student();
    ~komandadodaj_student();

    void promeniUsername(QString novo){Username=novo;}
    void promeniPassword(QString novo){Password=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);
private:

    QString Username="";
    QString Password="";
};

#endif // KOMANDADODAJ_STUDENT_H
