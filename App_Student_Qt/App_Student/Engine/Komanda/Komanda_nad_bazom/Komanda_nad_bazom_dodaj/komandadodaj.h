#ifndef KOMANDADODAJ_H
#define KOMANDADODAJ_H
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>
#include <QObject>
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"

class KomandaDodaj : public KomandaNadBazom
{
    Q_OBJECT
public:
    KomandaDodaj();
    virtual ~KomandaDodaj();

    Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza); //netreba virtual

    virtual QByteArray za_slanje()=0;
    virtual int praviOd(QString &izvorniQString)=0;

protected:
        Tipovi Tabela;


    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
};

#endif // KOMANDADODAJ_H
