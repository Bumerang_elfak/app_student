#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h"

KomandaDodaj::KomandaDodaj()
{
    Tip=KOMANDA_DODAJ;
    adresa_povratka=NULL;
}

KomandaDodaj::~KomandaDodaj()
{

}
Odgovor_na_Komandu *KomandaDodaj::izvrsiNadBazom(QSqlDatabase &baza)
{
    QSqlQuery query(baza);
    QString upit=toQuery();
    Odgovor_na_Komandu *k=NULL;
    //qDebug()<<upit;
    //std::cout<<upit.toStdString()<<std::endl;

    if(upit!= NULL && query.exec(upit)){
        //lepo izvrseno
        //sada prvo izvuci id zadnjeg iz te tabele
        //SELECT LAST(column_name) FROM table_name;
        //
        QSqlQuery query2(baza);
        upit="";
        upit.append("SELECT MAX(");
        switch(Tabela){
            case ADMIN:{
                upit.append(TABELA_ADMIN_ID);
                upit.append(") FROM ");
                upit.append(TABELA_ADMIN);
                break;
            }
            case KVIZ:{
                upit.append(TABELA_KVIZ_ID);
                upit.append(") FROM ");
                upit.append(TABELA_KVIZ);
                break;
            }
            case OBLAST:{
                upit.append(TABELA_OBLAST_ID);
                upit.append(") FROM ");
                upit.append(TABELA_OBLAST);
                break;
            }
            case PITANJE:{
                upit.append(TABELA_PITANJE_ID);
                upit.append(") FROM ");
                upit.append(TABELA_PITANJE);
                break;
            }
            case PREDMET:{
                upit.append(TABELA_PREDMET_ID);
                upit.append(") FROM ");
                upit.append(TABELA_PREDMET);
                break;
            }
            case PROFESOR:{
                upit.append(TABELA_PROFESOR_ID);
                upit.append(") FROM ");
                upit.append(TABELA_PROFESOR);
                break;
            }
            case STUDENT:{
                upit.append(TABELA_STUDENT_ID);
                upit.append(") FROM ");
                upit.append(TABELA_STUDENT);
                break;
            }
            default:{
            //xD ovo ovde ne znam kako definisati
            //ocigledno neka nebulozna greska
        }
        }
        upit.append(";");
        bool u_redu=query2.exec(upit);
        if( u_redu ){
            query2.next();
            int ID=query2.value(0).toInt();
            Odgovor_Snimljen *ok=new Odgovor_Snimljen();
            ok->postavi_ID(ID);
            ok->postavi_adresu_povratka(adresa_povratka);
            k=ok;
        }
        else{
            //dodat je objekat, ali jbg iz nekog razloga ne moze se
            //izvaditi ID iz objekta!!!
            Odgovor_Snimljen *ok=new Odgovor_Snimljen();
            ok->postavi_adresu_povratka(adresa_povratka);
            k=ok;
        }
    }
    else{
        //greska prilikom upita
        k=new Error();
        k->postavi_adresu_povratka(adresa_povratka);
        qobject_cast<Error*>(k)->postavi_greska("Greska prilikom dodavanja u bazu");
    }

    return k;
}
