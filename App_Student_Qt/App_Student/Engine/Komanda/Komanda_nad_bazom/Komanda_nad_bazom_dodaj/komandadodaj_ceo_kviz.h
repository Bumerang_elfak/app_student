#ifndef KOMANDADODAJ_CEO_KVIZ_H
#define KOMANDADODAJ_CEO_KVIZ_H

#include <QObject>
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/fje_za_koriscenje.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_kviz.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj_poseduje_pitanje.h"
#include "Engine/PPOK/kviz.h"

class Komandadodaj_ceo_kviz : public KomandaNadBazom
{
    Q_OBJECT
    Kviz k;

public:
    Komandadodaj_ceo_kviz();

    void postavi( Kviz k ){
        this->k=k;
    }
    Kviz vrati_kviz(){
        return k;
    }

    virtual Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza); //zameni posle za istancu baze, stavi sta treba
    virtual QString toQuery(){
        return "";
    }

    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString &izvorniBitArray);

};

#endif // KOMANDADODAJ_CEO_KVIZ_H
