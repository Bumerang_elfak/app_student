#include "komandadodaj_profesor.h"

komandadodaj_profesor::komandadodaj_profesor()
{
    Tabela=PROFESOR;
}

komandadodaj_profesor::~komandadodaj_profesor()
{

}
QString komandadodaj_profesor::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_PROFESOR)+" ("+QString(TABELA_PROFESOR_IME)+", "+QString(TABELA_PROFESOR_PREZIME);
    temp=temp+", "+QString(TABELA_PROFESOR_E_MAIL)+", "+QString(TABELA_PROFESOR_USERNAME)+", "+QString(TABELA_PROFESOR_PASSWORD)+") VALUES(";
    temp=temp+enkapsulirajZaBazu(Ime)+", "+enkapsulirajZaBazu(Prezime)+", "+enkapsulirajZaBazu(Email)+", "+enkapsulirajZaBazu(Username)+", "+enkapsulirajZaBazu(Password)+");";

    return temp;
}
QByteArray komandadodaj_profesor::za_slanje()
{
    // Tip || table || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(Ime));
    povratniString.append(enkapsuliraj(Prezime));
    povratniString.append(enkapsuliraj(Email));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_profesor::praviOd(QString &izvorniQString)
{
    // Tip || table || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PROFESOR)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPrezime(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniEmail(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}
