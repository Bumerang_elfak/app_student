#ifndef KOMANDADODAJ_ADMIN_H
#define KOMANDADODAJ_ADMIN_H

#include <QObject>
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>

#include "Engine/Komanda/Komanda.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_dodaj/komandadodaj.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"
#include "Engine/Odgovor_na_komandu/error.h"
#include "Engine/tabele_i_kolone_iz_baze.h"


class komandadodaj_admin : public KomandaDodaj
{
    Q_OBJECT
public:
    komandadodaj_admin();
    ~komandadodaj_admin();


    void promeniIme(QString novo) {Ime=novo;}
    void promeniPrezime(QString novo){Prezime=novo;}
    void promeniEmail(QString novo){Email=novo;}
    void promeniUsername(QString novo){Username=novo;}
    void promeniPassword(QString novo){Password=novo;}

    virtual QString toQuery();
    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniQString);

private:

    QString Ime="";
    QString Prezime="";
    QString Email="";
    QString Username="";
    QString Password="";

};

#endif // KOMANDADODAJ_ADMIN_H
