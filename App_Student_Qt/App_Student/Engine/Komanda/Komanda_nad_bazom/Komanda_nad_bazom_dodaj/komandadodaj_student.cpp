#include "komandadodaj_student.h"

komandadodaj_student::komandadodaj_student()
{
    Tabela=STUDENT;
}

komandadodaj_student::~komandadodaj_student()
{

}
QString komandadodaj_student::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_STUDENT)+" ("+QString(TABELA_STUDENT_USERNAME)+", "+QString(TABELA_STUDENT_PASSWORD)+") VALUES(";
    temp=temp+enkapsulirajZaBazu(Username)+", "+enkapsulirajZaBazu(Password)+");";

    return temp;
}
QByteArray komandadodaj_student::za_slanje()
{
    // Tip || table || Ime || Prezime || Email || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_student::praviOd(QString &izvorniQString)
{
    // Tip || table || Username || Password
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=STUDENT)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}
