#include "komandadodaj_kviz.h"

komandadodaj_kviz::komandadodaj_kviz()
{
    Tabela=KVIZ;
}
komandadodaj_kviz::~komandadodaj_kviz()
{

}
QString komandadodaj_kviz::toQuery()
{
    /*INSERT INTO table_name
VALUES (value1,value2,value3,...);*/
    using namespace Fje_za_Koriscenje;
    QString temp="INSERT INTO " + QString(TABELA_KVIZ)+" ("+QString(TABELA_KVIZ_IME)+", "+QString(TABELA_KVIZ_DINAMICKI);
    temp=temp+", "+QString(TABELA_KVIZ_TEZINA)+", "+QString(TABELA_KVIZ_BROJ_PITANJA)+", "+QString(TABELA_KVIZ_ID_PROFESOR)
            +", "+QString(TABELA_KVIZ_ID_PREDMET)+", "+QString(TABELA_KVIZ_ID_OBLASTI)+") VALUES(";
    temp=temp+enkapsulirajZaBazu(ImeKviza)+", "
            +enkapsulirajZaBazu(DinamickiKviz)+", "
            +enkapsulirajZaBazu(TezinaKviza)+", "
            +enkapsulirajZaBazu(BrojPitanja)+", "
            +enkapsulirajZaBazu(IdProfesora)+", "
            +enkapsulirajZaBazu(IdPredmeta)+", "
            +enkapsulirajZaBazu(IdOblasti)+");";

    return temp;
}
QByteArray komandadodaj_kviz::za_slanje()
{
    // Tip || Tabela || ImeKviza || DinamickiKviz || TezinaKviza || BrojPitanja
    // || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(Tabela)));
    povratniString.append(enkapsuliraj(ImeKviza));
    povratniString.append(enkapsuliraj(QString::number(DinamickiKviz)));
    povratniString.append(enkapsuliraj(QString::number(TezinaKviza)));
    povratniString.append(enkapsuliraj(QString::number(BrojPitanja)));
    povratniString.append(enkapsuliraj(QString::number(IdProfesora)));
    povratniString.append(enkapsuliraj(QString::number(IdPredmeta)));
    povratniString.append(enkapsuliraj(QString::number(IdOblasti)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}
int komandadodaj_kviz::praviOd(QString &izvorniQString)
{
    // Tip || Tabela || ImeKviza || DinamickiKviz || TezinaKviza || BrojPitanja
    // || IdProfesora || IdPredmeta || IdOblasti
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ)
        return 0;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KVIZ)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_DODAJ;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniDinamicki(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promenitezinu(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniBrojPitanja(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdProfesora(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdPredmeta(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIdOblasti(pomocna.toInt());
    return iterator;
}
