#include "komandadodaj_ceo_kviz.h"

Komandadodaj_ceo_kviz::Komandadodaj_ceo_kviz(){
    Tip=KOMANDA_DODAJ_CEO_KVIZ;
    k.postavi_ID(-10);//namerno
}
Odgovor_na_Komandu* Komandadodaj_ceo_kviz::izvrsiNadBazom(QSqlDatabase &baza){
    komandadodaj_kviz kom;
    kom.promeniBrojPitanja(k.vrati_pitanja().count());
    kom.promeniIme(k.vrati_ime());
    kom.promeniDinamicki(k.vrati_dinamicki());
    kom.promeniIdPredmeta(k.vrati_ID_Predmeta());
    kom.promeniIdOblasti(k.vrati_ID_Oblasti());
    kom.promeniIdProfesora(k.vrati_ID_Profesora());
    kom.promenitezinu(k.vrati_tezinu());
    baza.transaction();
    Odgovor_na_Komandu *odg=kom.izvrsiNadBazom(baza);
    if( odg->vrati_tip()==ERROR ){
        Error *e=new Error();
        e->postavi_greska("Kviz nije dodat ne mogu nastaviti dalje");
        baza.rollback();
        return e;
    }
    //dodat kviz treba popuniti drugu tabelu
    Odgovor_Snimljen *odg_snim_kviz=qobject_cast<Odgovor_Snimljen*>(odg);
    int ID_dodatog_kviza=odg_snim_kviz->vrati_ID();
    for( int i=0; i<k.vrati_pitanja().count(); i++ ){
        komandadodaj_poseduje_pitanje komanda;
        komanda.promeniIdKviza(ID_dodatog_kviza);
        komanda.promeniIdPitanja(k.vrati_pitanja()[i].vrati_ID());
        Odgovor_na_Komandu *odgovor=komanda.izvrsiNadBazom(baza);
        if( odgovor->vrati_tip()==ERROR ){
            Error *e=new Error();
            e->postavi_greska("Pitanje " + QString(i) + " nije lepo dodato!");
            baza.rollback();
            delete odg;
            delete odgovor;
            return e;
        }
        delete odgovor;
    }
    baza.commit();
    return odg;
}
QByteArray Komandadodaj_ceo_kviz::za_slanje(){
    // TIP || KVIZ
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(Tip)));
    stringZaSlanje.append(enkapsuliraj(k.za_slanje()));

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}
int        Komandadodaj_ceo_kviz::praviOd(QString &izvorniBitArray){

    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    if(pomocna.toInt()!=KOMANDA_DODAJ_CEO_KVIZ)
        return 0;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    k.pravi_od(pomocna);

    return iterator;

}
