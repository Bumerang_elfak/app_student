#include "Engine/Komanda/Komanda_nad_bazom/komandabrisi.h"

KomandaBrisi::KomandaBrisi()
{
    Tip=KOMANDA_BRISI;
}
KomandaBrisi::~KomandaBrisi()
{

}

void KomandaBrisi::PromeniUslov(QString ImeIdaUTabeli, int vrednostIdaUTabeli, int poseduje_pitanje_IdPitanja /*=-1*/)
{
    IdTabele=ImeIdaUTabeli;
    vrednostTabele=vrednostIdaUTabeli;
    vrednostIdPitanja=poseduje_pitanje_IdPitanja;
}

QString KomandaBrisi::toQuery()
{
    QString upit;
    upit="Delete from "+tabela +
            " Where " + IdTabele+ "="+QString::number(vrednostTabele);
    if(tabela!=QString(TABELA_POSEDUJE_PITANJE))
    {
        upit+=";";
    }
    else
    {
        upit+=  " AND "
                +QString(TABELA_POSEDUJE_PITANJE_ID_PITANJA)
                +"="+QString::number(vrednostIdPitanja)+";";
    }
    return upit;
}

Odgovor_na_Komandu *KomandaBrisi::izvrsiNadBazom(QSqlDatabase& baza)
{
    QSqlQuery query(baza);

    QString upit=toQuery();
    Odgovor_na_Komandu *k=NULL;

    //std::cout<<upit.toStdString()<<std::endl;
    if(upit!=NULL && upit!="" && query.exec(upit)){
        //lepo izvrseno
        k=new Odgovor_Obrisan();
        k->postavi_adresu_povratka(adresa_povratka);
        return k;
    }
    else{
        //greska prilikom upita
        k=new Error();
        k->postavi_adresu_povratka(adresa_povratka);
        qobject_cast<Error*>(k)->postavi_greska("Greska prilikom brisanja, nije obrisano");
        return k;
    }
    return NULL;
}

QByteArray KomandaBrisi::za_slanje()
{
    // Tip || tabela || IdTabele || vrednostTabele || vrednostIdPitanja
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(tabela));
    povratniString.append(enkapsuliraj(IdTabele));
    povratniString.append(enkapsuliraj(QString::number(vrednostTabele)));
    povratniString.append(enkapsuliraj(QString::number(vrednostIdPitanja)));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}

int KomandaBrisi::praviOd(QString &izvorniQString)
{
    // Tip || tabela || IdTabele || vrednostTabele || vrednostIdPitanja
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=KOMANDA_BRISI)
        return 0;

    //dekapsulacija
    Tip=KOMANDA_BRISI;
    pomocna=dekapsulacija(izvorniQString,iterator);
    PromeniTabelu(pomocna);
    int temp1,temp2;
    pomocna=dekapsulacija(izvorniQString,iterator);

    temp1=dekapsulacija(izvorniQString,iterator).toInt();
    temp2=dekapsulacija(izvorniQString,iterator).toInt();
    PromeniUslov(pomocna,temp1,temp2);
    return iterator;
}
