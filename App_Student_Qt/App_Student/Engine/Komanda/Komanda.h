#ifndef KOMANDA_H
#define KOMANDA_H

//Posiljaoc se nigde ne koristi, moze se dodati
//zbog povecanja sigurnosti sistema
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>
#include <QTcpSocket>
#include <QObject>
#include "Engine/Enums.h"
#include "Engine/Osobe/osobe.h"
#include "Engine/Osobe/profesor.h"
#include "Engine/Osobe/admin.h"
#include "Engine/Osobe/student.h"




class Komanda : public QObject
{
    Q_OBJECT

protected:
    Tipovi Tip;
    Osobe*   posiljaoc=NULL;             //Ko je poslao Komandu
    QTcpSocket *adresa_povratka=NULL;    //POINTER
    //prioritet -- zavisi od posiljaoca

    virtual void brisi_mem();
    virtual void pravi_od(Komanda& k);

public:
    explicit    Komanda(QObject *parent = 0);
    Komanda(Komanda& k);
    virtual     ~Komanda();

    void        postavi_posiljaoc( Osobe *o );
    void        postavi_adresa_povratka(QTcpSocket* adresa_pov );
    Osobe*      vrati_posiljaoc();
    QTcpSocket* vrati_adresa_povratka();
    int         vrati_prioritet();

    void            SaljiNaPort(QTcpSocket *sock);
    QByteArray      prikazi_sta_se_salje();

    int                 vrati_tip();
    virtual QByteArray  za_slanje()=0;
    virtual int        praviOd(QString &s)=0;

};

#endif // KOMANDA_H
