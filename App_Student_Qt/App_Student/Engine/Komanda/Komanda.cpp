#include "Engine/Komanda/Komanda.h"


//constructors && desrtuctors
Komanda::Komanda(QObject *parent) : QObject(parent){
    adresa_povratka=NULL;
}
Komanda::Komanda(Komanda& k):QObject(NULL){
    pravi_od(k);
}
Komanda::~Komanda(){
    brisi_mem();
}

//functions
//protected
void        Komanda::brisi_mem(){
    delete posiljaoc;
}
void        Komanda::pravi_od(Komanda& k){
    //Tipovi Tip;
    //Osobe*   posiljaoc;
    //QTcpSocket *adresa_povratka;D
    Tip=k.Tip;
    postavi_posiljaoc(k.posiljaoc);
    adresa_povratka= k.adresa_povratka;
}

//public
void        Komanda::postavi_posiljaoc( Osobe *o ){
    switch( o->vrati_tip() ){
    case ADMIN:     posiljaoc=new Admin(*(qobject_cast<Admin *>(o)));
                    break;
    case PROFESOR:  posiljaoc=new Profesor(*(qobject_cast<Profesor *>(o)));
                    break;
    case STUDENT:   posiljaoc=new Student(*(qobject_cast<Student *>(o)));
                    break;
    }
}
void        Komanda::postavi_adresa_povratka( QTcpSocket* adresa_pov ){
    adresa_povratka=adresa_pov;
}
Osobe*      Komanda::vrati_posiljaoc(){
    return posiljaoc;
}
QTcpSocket* Komanda::vrati_adresa_povratka(){
    return adresa_povratka;
}
int         Komanda::vrati_prioritet(){
    return Tip-KOMANDA_UPIT;
}
void        Komanda::SaljiNaPort(QTcpSocket *sock){
    QByteArray podaci=za_slanje();
    int duzina_pod=podaci.size();

    sock->write((char *)&duzina_pod,sizeof(int));
    sock->write(podaci);
}
QByteArray  Komanda::prikazi_sta_se_salje(){
    QByteArray podaci=za_slanje();
    int duzina_pod=podaci.size();
    podaci.prepend((char*)&duzina_pod,sizeof(int));
    return podaci;
}
int         Komanda::vrati_tip(){
    return Tip;
}

