#ifndef KOMANDA_LOGOVANJE_H
#define KOMANDA_LOGOVANJE_H

#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/Komanda/KomandaNadBazom.h"
#include "Engine/fje_za_koriscenje.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_logovanje.h"
#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/Odgovor_na_komandu/error.h"

class Komanda_Logovanje : public KomandaNadBazom
{
    Q_OBJECT
private:
    QString user="";
    QString pass="";
    Tipovi tipOsobe; //sadrzi u kojoj tabeli se trazi osoba

public:
    Komanda_Logovanje();
    ~Komanda_Logovanje();

    void postavi_user(QString u){
        user=u;
    }
    void postavi_pass(QString u){
        pass=u;
    }
    void postavi_tabelu(Tipovi noviTip)
    {
        tipOsobe=noviTip;
    }

    QString vrati_user(){
        return user;
    }
    QString vrati_pass(){
        return pass;
    }
    Tipovi vrati_tabelu()
    {
        return tipOsobe;
    }


    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString &izvorniBitArray);
    Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza); //zameni posle za istancu baze, stavi sta treba
    QString toQuery();

};

#endif // KOMANDA_LOGOVANJE_H
