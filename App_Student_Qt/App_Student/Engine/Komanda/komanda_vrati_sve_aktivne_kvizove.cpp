#include "komanda_vrati_sve_aktivne_kvizove.h"

komanda_vrati_sve_aktivne_kvizove::komanda_vrati_sve_aktivne_kvizove()
{
    Tip=KOMANDA_VRATI_SVE_AKTIVNE_KVIZOVE;
}

komanda_vrati_sve_aktivne_kvizove::~komanda_vrati_sve_aktivne_kvizove()
{

}

QByteArray  komanda_vrati_sve_aktivne_kvizove::za_slanje(){
    // TIP
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int komanda_vrati_sve_aktivne_kvizove::praviOd(QString &s){

    // TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_VRATI_SVE_AKTIVNE_KVIZOVE )
        return 0;

    return iterator;
}
