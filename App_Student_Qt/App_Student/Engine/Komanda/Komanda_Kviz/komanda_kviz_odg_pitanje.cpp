#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_odg_pitanje.h"

//Tipovi Tip;
//Osobe*   posiljaoc;             //Ko je poslao Komandu
//QTcpSocket *adresa_povratka;
//int ID_Aktivan_kviz;
//bool tacan

Komanda_Kviz_Odg_Pitanje::Komanda_Kviz_Odg_Pitanje(){
    Tip=KOMANDA_KVIZ_ODGOVOR_NA_PITANJE;
}
Komanda_Kviz_Odg_Pitanje::~Komanda_Kviz_Odg_Pitanje(){}
QByteArray  Komanda_Kviz_Odg_Pitanje::za_slanje(){

    // Tip || ID_AKT_KVIZ || Tacan(0,1)
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_Aktivan_kviz)));
    if( tacan==true )
        s.append(enkapsuliraj(QString::number(1)));
    else
        s.append(enkapsuliraj(QString::number(0)));

    QByteArray ba;
    ba.append(s);
    return ba;

}
int        Komanda_Kviz_Odg_Pitanje::praviOd(QString &s){

    // Tip || ID_AKT_KVIZ || Tacan(0,1)
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_KVIZ_ODGOVOR_NA_PITANJE )
        return 0;

    pom=dekapsulacija(s,iterator);
    ID_Aktivan_kviz=pom.toInt();

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()==1 )
        tacan=true;
    else
        tacan=false;

    return iterator;
}
