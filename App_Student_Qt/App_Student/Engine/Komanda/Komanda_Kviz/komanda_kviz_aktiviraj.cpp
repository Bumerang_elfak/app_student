#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_aktiviraj.h"

//Kviz kviz_za_aktivaciju;
//Tipovi Tip;
//Osobe*   posiljaoc;             //Ko je poslao Komandu
//QTcpSocket *adresa_povratka;
//int ID_Aktivan_kviz;

Komanda_Kviz_Aktiviraj::Komanda_Kviz_Aktiviraj(){
    Tip=KOMANDA_KVIZ_AKTIVIRAJ_KVIZ;
}
Komanda_Kviz_Aktiviraj::~Komanda_Kviz_Aktiviraj(){}
void        Komanda_Kviz_Aktiviraj::postavi_kviz_za_aktivaciju( Kviz k ){
    kviz_za_aktivaciju=k;
}
QByteArray  Komanda_Kviz_Aktiviraj::za_slanje(){

    //TIP || KVIZ || ID_Aktivan_kviz
    using namespace Fje_za_Koriscenje;

    QString s;
    ID_Aktivan_kviz=kviz_za_aktivaciju.vrati_ID();
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(kviz_za_aktivaciju.za_slanje()));
    s.append(enkapsuliraj(QString::number(ID_Aktivan_kviz)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int        Komanda_Kviz_Aktiviraj::praviOd(QString &s){

    //TIP || KVIZ || ID_AKTIVAN_KVIZ
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_KVIZ_AKTIVIRAJ_KVIZ )
        return 0;

    pom=dekapsulacija(s,iterator);
    QString ba;
    ba.append(pom);
    kviz_za_aktivaciju.pravi_od(ba);

    pom=dekapsulacija(s,iterator);
    ID_Aktivan_kviz=pom.toInt();
    return iterator;
}
