#ifndef KOMANDA_KVIZ_PRIKAZI_STATISTIKU_H
#define KOMANDA_KVIZ_PRIKAZI_STATISTIKU_H

//TIP || ID_AKT_KVIZ

#include "Engine/Komanda/komanda_kviz.h"

class Komanda_Kviz_Prikazi_Statistiku : public Komanda_Kviz
{
    Q_OBJECT
public:
    Komanda_Kviz_Prikazi_Statistiku();
    ~Komanda_Kviz_Prikazi_Statistiku();

    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString &s);
};

#endif // KOMANDA_KVIZ_PRIKAZI_STATISTIKU_H
