#ifndef KOMANDA_KVIZ_NASILNO_PREKINI_H
#define KOMANDA_KVIZ_NASILNO_PREKINI_H

#include "Engine/Komanda/komanda_kviz.h"

class Komanda_Kviz_Nasilno_Prekini:public Komanda_Kviz
{
    Q_OBJECT
public:
    Komanda_Kviz_Nasilno_Prekini();
    ~Komanda_Kviz_Nasilno_Prekini();

    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString &s);
};

#endif // KOMANDA_KVIZ_NASILNO_PREKINI_H
