#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_log.h"

//int ID_Aktivan_kviz;
//Tipovi Tip;
//Osobe*   posiljaoc;             //Ko je poslao Komandu
//QTcpSocket *adresa_povratka;
//int sifra_kviza;


Komanda_Kviz_Log::Komanda_Kviz_Log(){
    Tip=KOMANDA_KVIZ_LOGUJ_SE_NA_KVIZ;
}
QByteArray  Komanda_Kviz_Log::za_slanje(){

    //TIP  || sifra_kviza
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(sifra_kviza)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int        Komanda_Kviz_Log::praviOd(QString &s){

    //TIP  || sifra_kviza
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_KVIZ_LOGUJ_SE_NA_KVIZ )
        return 0;

    pom=dekapsulacija(s,iterator);
    sifra_kviza=pom.toInt();

    return iterator;
}
