#ifndef KOMANDA_KVI_ODG_PITANJE_H
#define KOMANDA_KVI_ODG_PITANJE_H

// Tip || ID_AKT_KVIZ || Tacan(0,1)

#include <QObject>
#include "Engine/Komanda/komanda_kviz.h"

class Komanda_Kviz_Odg_Pitanje : public Komanda_Kviz
{
    Q_OBJECT
private:
    bool tacan=false;
    //Tipovi Tip;
    //Osobe*   posiljaoc;             //Ko je poslao Komandu
    //QTcpSocket *adresa_povratka;
    //int ID_Aktivan_kviz;

public:
    Komanda_Kviz_Odg_Pitanje();
    ~Komanda_Kviz_Odg_Pitanje();

    bool vrati_tacan(){
        return tacan;
    }
    void postavi_tacan(bool t){
        tacan=t;
    }
    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString &s);
};

#endif // KOMANDA_KVI_ODG_PITANJE_H
