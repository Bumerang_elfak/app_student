#ifndef KOMANDA_KVIZ_LOG_H
#define KOMANDA_KVIZ_LOG_H

//Salje
// TIP || SIFRA
//salje samo sifru pa se po sifri trazi

#include <QObject>
#include "Engine/Komanda/komanda_kviz.h"

class Komanda_Kviz_Log : public Komanda_Kviz
{

    Q_OBJECT
private:
    //int ID_Aktivan_kviz;
    //Tipovi Tip;
    //Osobe*   posiljaoc;             //Ko je poslao Komandu
    //QTcpSocket *adresa_povratka;

    int sifra_kviza=0;
public:
    Komanda_Kviz_Log();
    ~Komanda_Kviz_Log(){}


    void    postavi_sifru_kviza(int sifra){
        sifra_kviza=sifra;
    }
    int     vrati_sifru_kviza(){
        return sifra_kviza;
    }
    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString &s);
};

#endif // KOMANDA_KVIZ_LOG_H
