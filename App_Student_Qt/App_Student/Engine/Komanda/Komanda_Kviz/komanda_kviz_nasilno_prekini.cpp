#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_nasilno_prekini.h"

Komanda_Kviz_Nasilno_Prekini::Komanda_Kviz_Nasilno_Prekini(){
    Tip=KOMANDA_KVIZ_NASILAN_PREKID;
}
Komanda_Kviz_Nasilno_Prekini::~Komanda_Kviz_Nasilno_Prekini(){}
QByteArray  Komanda_Kviz_Nasilno_Prekini::za_slanje(){
    // Tip || ID_AKT_KVIZ
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_Aktivan_kviz)));

    QByteArray ba;
    ba.append(s);
    return ba;
}
int        Komanda_Kviz_Nasilno_Prekini::praviOd(QString &s){
    // Tip || ID_AKT_KVIZ
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_KVIZ_NASILAN_PREKID )
        return 0;

    pom=dekapsulacija(s,iterator);
    ID_Aktivan_kviz=pom.toInt();

    return iterator;
}
