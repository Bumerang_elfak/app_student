#ifndef KOMANDA_KVIZ_SL_PITANJE_H
#define KOMANDA_KVIZ_SL_PITANJE_H

//Salje
// TIP || ID_AKT_KVIZ

#include <QObject>
#include "Engine/Komanda/komanda_kviz.h"

class Komanda_Kviz_sl_Pitanje : public Komanda_Kviz
{
    Q_OBJECT

public:
    Komanda_Kviz_sl_Pitanje();
    ~Komanda_Kviz_sl_Pitanje(){}
    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString &s);
};

#endif // KOMANDA_KVIZ_SL_PITANJE_H
