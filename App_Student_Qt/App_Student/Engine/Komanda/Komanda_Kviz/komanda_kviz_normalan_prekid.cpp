#include "Engine/Komanda/Komanda_Kviz/komanda_kviz_normalan_prekid.h"

Komanda_Kviz_Normalan_Prekid::Komanda_Kviz_Normalan_Prekid(){
    Tip=KOMANDA_KVIZ_NORMALAN_PREKID;
}
Komanda_Kviz_Normalan_Prekid::~Komanda_Kviz_Normalan_Prekid(){}

QByteArray  Komanda_Kviz_Normalan_Prekid::za_slanje(){

    //TIP || ID_AKT_KVIZ
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_Aktivan_kviz)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int        Komanda_Kviz_Normalan_Prekid::praviOd(QString &s){

    // TIP || ID_AKT_KVIZ
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_KVIZ_NORMALAN_PREKID )
        return 0;

    pom=dekapsulacija(s,iterator);
    ID_Aktivan_kviz=pom.toInt();
    return iterator;
}
