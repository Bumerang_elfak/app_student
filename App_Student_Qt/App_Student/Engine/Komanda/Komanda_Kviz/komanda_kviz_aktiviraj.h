#ifndef KOMANDA_KVIZ_AKTIVIRAJ_H
#define KOMANDA_KVIZ_AKTIVIRAJ_H

//Salje
//TIP || KVIZ

#include <QObject>
#include "Engine/Komanda/komanda_kviz.h"
#include "Engine/PPOK/kviz.h"

class Komanda_Kviz_Aktiviraj : public Komanda_Kviz
{
    Q_OBJECT
private:
    Kviz kviz_za_aktivaciju;
    //Tipovi Tip;
    //Osobe*   posiljaoc;             //Ko je poslao Komandu
    //QTcpSocket *adresa_povratka;
    //int ID_Aktivan_kviz;

public:
    Komanda_Kviz_Aktiviraj();
    ~Komanda_Kviz_Aktiviraj();

    Kviz                vrati_kviz(){
        return kviz_za_aktivaciju;
    }
    void                postavi_kviz_za_aktivaciju( Kviz k );
    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString &s);
};

#endif // KOMANDA_KVIZ_AKTIVIRAJ_H
