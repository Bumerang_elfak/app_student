#ifndef KOMANDA_VRATI_SVE_AKTIVNE_KVIZOVE_H
#define KOMANDA_VRATI_SVE_AKTIVNE_KVIZOVE_H

#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

#include "Engine/tabele_i_kolone_iz_baze.h"
#include "Engine/Odgovor_na_komandu/error.h"

class komanda_vrati_sve_aktivne_kvizove : public Komanda
{
public:
    komanda_vrati_sve_aktivne_kvizove();
    ~komanda_vrati_sve_aktivne_kvizove();

    virtual QByteArray za_slanje();
    virtual int praviOd(QString &izvorniBitArray);
};

#endif // KOMANDA_VRATI_SVE_AKTIVNE_KVIZOVE_H
