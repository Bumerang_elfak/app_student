#include "Engine/Komanda/komanda_logovanje.h"

Komanda_Logovanje::Komanda_Logovanje(){
    Tip=KOMANDA_LOGOVANJE;
}
Komanda_Logovanje::~Komanda_Logovanje(){}

QByteArray  Komanda_Logovanje::za_slanje(){
    // TIP || USER || PASS || TipOsobe
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(user));
    s.append(enkapsuliraj(pass));
    s.append(enkapsuliraj(QString::number(tipOsobe)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int Komanda_Logovanje::praviOd(QString &s){

    // TIP || USER || PASS || TipOsobe
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;

    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_LOGOVANJE )
        return 0;

    pom=dekapsulacija(s,iterator);
    user=pom;

    pom=dekapsulacija(s,iterator);
    pass=pom;

    pom=dekapsulacija(s,iterator);
    tipOsobe=Tipovi(pom.toInt());

    return iterator;
}

Odgovor_na_Komandu* Komanda_Logovanje::izvrsiNadBazom(QSqlDatabase &baza)
{
    QSqlQuery query(baza);
    QString upit=toQuery();
    Odgovor_na_Komandu *odgovor=NULL;
    std::cout<<upit.toStdString()<<std::endl;
    if(query.exec(upit)){
        //lepo izvrseno
        if(query.next()){
            odgovor=new odgovor_logovanje(tipOsobe);
            odgovor_logovanje *Odgovor=qobject_cast<odgovor_logovanje*> (odgovor);
            switch(tipOsobe)
            {
                case PROFESOR:
                {
                    std::cout<<query.value(0).toInt()<<" "<<query.value(1).toString().toStdString()<<
                               " "<<query.value(2).toString().toStdString()<<
                               " "<<query.value(3).toString().toStdString()
                             <<" "<<query.value(4).toString().toStdString()<<" "<<std::endl;
                    Odgovor->promeniId(query.value(TABELA_INDEX_PROFESOR_ID).toInt());
                    Odgovor->promeniIme(query.value(TABELA_INDEX_PROFESOR_IME).toString());
                    Odgovor->promeniPrezime(query.value(TABELA_INDEX_PROFESOR_PREZIME).toString());
                    Odgovor->promeniEmail(query.value(TABELA_INDEX_PROFESOR_E_MAIL).toString());
                    Odgovor->promeniUsername(query.value(TABELA_INDEX_PROFESOR_USERNAME).toString());
                    //glupo je da vracamo password
                    break;
                }
                case ADMIN:
                {

                    Odgovor->promeniId(query.value("IdAdmina").toInt());
                    Odgovor->promeniIme(query.value("Ime").toString());
                    Odgovor->promeniPrezime(query.value("Prezime").toString());
                    Odgovor->promeniEmail(query.value("E_mail").toString());
                    Odgovor->promeniUsername(query.value(TABELA_PROFESOR_USERNAME).toString());
                    //glupo je da vracamo password
                    break;
                }
                case STUDENT:
                {
                    Odgovor->promeniId(query.value(TABELA_STUDENT_ID).toInt());
                    Odgovor->promeniUsername(query.value(TABELA_STUDENT_USERNAME).toString());
                    //glupo je da vracamo password
                    break;
                }
                default:{break;}
            }
        }
        else{
            odgovor=new Error();
            odgovor->postavi_adresu_povratka(adresa_povratka);
            qobject_cast<Error*>(odgovor)->postavi_greska("Ne valja username ili password");
        }
        odgovor->postavi_adresu_povratka(adresa_povratka);
    }
    else{
        //greska prilikom upita
        odgovor=new Error();
        odgovor->postavi_adresu_povratka(adresa_povratka);
        qobject_cast<Error*>(odgovor)->postavi_greska("Greska, Sistemska Greska prijavite gresku adminu");
    }

    return odgovor;
}
QString Komanda_Logovanje::toQuery()
{

    QString temp="Select * from ";
    switch(tipOsobe)
    {
    case(ADMIN):
    {
        temp+="admin where BINARY ";
        temp=temp+ " admin.Username="+Fje_za_Koriscenje::enkapsulirajZaBazu(user)+
                " AND  BINARY admin.Password="+Fje_za_Koriscenje::enkapsulirajZaBazu(pass)+";";
        break;
    }
    case(STUDENT):
    {
        temp=temp + TABELA_STUDENT +" where BINARY ";
        temp=temp+ TABELA_STUDENT_USERNAME+"="+Fje_za_Koriscenje::enkapsulirajZaBazu(user)
                + " AND BINARY "+TABELA_STUDENT_PASSWORD+"="+Fje_za_Koriscenje::enkapsulirajZaBazu(pass)+";";
        break;
    }
    case(PROFESOR):
    {
        temp=temp + TABELA_PROFESOR +" where BINARY ";
        temp=temp+ TABELA_PROFESOR_USERNAME+"="+Fje_za_Koriscenje::enkapsulirajZaBazu(user)
                + " AND BINARY "+TABELA_PROFESOR_PASSWORD+"="+Fje_za_Koriscenje::enkapsulirajZaBazu(pass)+";";
        break;
    }
    default: {break;}
    }
    return temp;
}
