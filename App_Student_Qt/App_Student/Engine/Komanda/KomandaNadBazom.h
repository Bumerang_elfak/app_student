#ifndef KOMANDANADBAZOM_H
#define KOMANDANADBAZOM_H

#include <QObject>
#include "Engine/Enums.h"
#include <QString>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDatabase>
#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class KomandaNadBazom : public Komanda
{
    Q_OBJECT
private:
    int povrtana_vrednost=0;

public:
    KomandaNadBazom();
    ~KomandaNadBazom();
    virtual Odgovor_na_Komandu* izvrsiNadBazom(QSqlDatabase &baza)=0; //zameni posle za istancu baze, stavi sta treba
    virtual QString toQuery()=0;

    virtual QByteArray  za_slanje()=0;
    virtual int        praviOd(QString &izvorniBitArray)=0;
};

#endif // KOMANDANADBAZOM_H
