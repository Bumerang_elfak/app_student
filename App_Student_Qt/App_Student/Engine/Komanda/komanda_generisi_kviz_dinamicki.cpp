#include "Engine/Komanda/komanda_generisi_kviz_dinamicki.h"

//int tezina;
//int ID_OBLAST;
//QString upit_koji_se_treba_izvrsiti;

Komanda_Generisi_Kviz_Dinamicki::Komanda_Generisi_Kviz_Dinamicki(){
    Tip=KOMANDA_GENERISI_KVIZ_DINAMICKI;
}
Komanda_Generisi_Kviz_Dinamicki::~Komanda_Generisi_Kviz_Dinamicki(){}

QByteArray  Komanda_Generisi_Kviz_Dinamicki::za_slanje(){
    // TIP || ID_OBLAST || TEZINA || BR_PITANJA
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_OBLAST)));
    s.append(enkapsuliraj(QString::number(tezina)));
    s.append(enkapsuliraj(QString::number(br_pitanja)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int        Komanda_Generisi_Kviz_Dinamicki::praviOd(QString &s){

    // TIP || ID_OBLAST || TEZINA || BR_PITANJA
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pom;
    pom=dekapsulacija(s,iterator);
    if( pom.toInt()!=KOMANDA_GENERISI_KVIZ_DINAMICKI )
        return 0;

    pom=dekapsulacija(s,iterator);
    ID_OBLAST=pom.toInt();

    pom=dekapsulacija(s,iterator);
    tezina=pom.toInt();

    pom=dekapsulacija(s,iterator);
    br_pitanja=pom.toInt();

    return iterator;
}

Odgovor_na_Komandu* Komanda_Generisi_Kviz_Dinamicki::izvrsiNadBazom(QSqlDatabase &baza) //zameni posle za istancu baze, stavi sta treba
{
    QSqlQuery query(baza);
    QString upit=toQuery();
    Odgovor_na_Komandu *odgovor=NULL;

    int i=0;
    if(query.exec(upit))
    {
        odgovor=new odgovor_generisi_kviz_dinamicki();
        odgovor_generisi_kviz_dinamicki *Odgovor=qobject_cast<odgovor_generisi_kviz_dinamicki*> (odgovor);
        while(query.next() && i<this->br_pitanja )
        {
            Pitanje *novoPitanje=new Pitanje();
            novoPitanje->postavi_ID(query.value(TABELA_INDEX_PITANJE_ID).toInt());
            novoPitanje->postavi_ID_Oblasti(query.value(TABELA_INDEX_PITANJE_ID_OBLASTI).toInt());
            novoPitanje->postavi_ID_Predmeta(query.value(TABELA_INDEX_PITANJE_ID_PREDMETA).toInt());
            novoPitanje->postavi_ID_Profesora(query.value(TABELA_INDEX_PITANJE_ID_PROFESORA).toInt());
            novoPitanje->postavi_tekst(query.value(TABELA_INDEX_PITANJE_TEKST).toString());
            novoPitanje->postavi_tacan(query.value(TABELA_INDEX_PITANJE_TACAN).toInt());
            novoPitanje->postavi_tezinu(query.value(TABELA_INDEX_PITANJE_TEZINA).toInt());

            novoPitanje->dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG1).toString());
            novoPitanje->dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG2).toString());
            novoPitanje->dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG3).toString());
            novoPitanje->dodaj_odg(query.value(TABELA_INDEX_PITANJE_ODG4).toString());

            Odgovor->dodajPitanje(*novoPitanje);
            delete novoPitanje;
            i++;
        }
    }
    else
    {
        //greska prilikom upita
        odgovor=new Error();
        odgovor->postavi_adresu_povratka(adresa_povratka);
        qobject_cast<Error*>(odgovor)->postavi_greska("Greska! Sistemska Greska prijavite adminu");
    }
    return odgovor;
}

QString Komanda_Generisi_Kviz_Dinamicki::toQuery()
{
    QString temp="Select * from "+
            QString(TABELA_PITANJE)+ " where "
            +QString(TABELA_PITANJE_TEZINA)+"<="+QString::number(tezina);
    temp=temp+" and "+QString(TABELA_PITANJE_ID_OBLASTI)+"="+QString::number(ID_OBLAST)+";";
    return temp;
}
