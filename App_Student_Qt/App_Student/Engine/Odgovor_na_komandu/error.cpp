#include "Engine/Odgovor_na_komandu/error.h"

Error::Error(){
    Tip=ERROR;
    OK=false;
}

QByteArray  Error::za_slanje(){
    // TIP || greska
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(greska));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int         Error::praviOd(QString& s){
    // TIP || greska
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if( pomocna.toInt()!=ERROR )
        return 0;

    pomocna=dekapsulacija(s,iterator);
    greska=pomocna;\
    return iterator;

}
