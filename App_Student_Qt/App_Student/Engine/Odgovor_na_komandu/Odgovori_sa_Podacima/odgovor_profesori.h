#ifndef ODGOVOR_PROFESORI_H
#define ODGOVOR_PROFESORI_H

#include <QObject>
#include <QString>
#include <QList>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

#include "Engine/Osobe/profesor.h"
#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/pitanje.h"

class Odgovor_Profesori : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Profesori();
    ~Odgovor_Profesori();

    QList<Profesor> vratiListu()    {return listaProfesora;}

    void dodaj_profesor(Profesor &p){
        listaProfesora.append(p);
    }
    void brisi_profesor(Profesor &p){
        int i=listaProfesora.indexOf(p);
        if( i==-1 )
            return;
        listaProfesora.removeAt(i);
    }

    QByteArray  za_slanje();
    int praviOd(QString& a);

private:
    QList<Profesor> listaProfesora;
};

#endif // ODGOVOR_PROFESORI_H
