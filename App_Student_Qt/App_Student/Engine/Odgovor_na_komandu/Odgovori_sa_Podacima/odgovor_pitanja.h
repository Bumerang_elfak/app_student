#ifndef ODGOVOR_PITANJA_H
#define ODGOVOR_PITANJA_H

#include <QObject>
#include <QString>
#include <QList>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

#include "Engine/PPOK/pitanje.h"
#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/pitanje.h"

class Odgovor_Pitanja : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Pitanja();
    ~Odgovor_Pitanja();

    QList<Pitanje> vratiListu()    {return listaPitanja;}

    void dodaj_pitanje(Pitanje &p){
        listaPitanja.append(p);
    }
    void brisi_pitanje(Pitanje &p){
        int i=listaPitanja.indexOf(p);
        if( i==-1 )
            return;
        listaPitanja.removeAt(i);
    }

    QByteArray  za_slanje();
    int praviOd(QString& izvorniBitArray);

private:
    QList<Pitanje> listaPitanja;
};

#endif // ODGOVOR_PITANJA_H
