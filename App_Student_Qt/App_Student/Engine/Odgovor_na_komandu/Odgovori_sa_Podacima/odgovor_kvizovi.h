#ifndef ODGOVOR_KVIZOVI_H
#define ODGOVOR_KVIZOVI_H

#include <QObject>
#include <QString>
#include <QList>

#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

#include "Engine/PPOK/kviz.h"
#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/pitanje.h"


class Odgovor_Kvizovi : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Kvizovi();
    ~Odgovor_Kvizovi();

    QList<Kviz> vratiListu()    {return listaKvizova;}
    // xD ovo ne valja za mnogo stvari
    //kao prvo kako da dodam kviz? seti SE C++ ne C#
    //kao drugo kako da izbrisem kviz?
    //kao trece vremenski ne valja poziva CopyConstruktor i vraca nov objekat!
    //dobra stvar kod ovog je sto vraca listu svih pitanja! samo to xD
    void dodaj_kviz( Kviz &k ){
        listaKvizova.append(k);
    }
    void brisi_kviz( Kviz &k ){
        int i=listaKvizova.indexOf(k);
        if( i==-1 )
            return;//nema brisanja to ne postoji
        listaKvizova.removeAt(i);
    }

    QByteArray  za_slanje();
    int praviOd(QString& izvorniBitArray);

private:
    QList<Kviz> listaKvizova;


};

#endif // ODGOVOR_KVIZOVI_H
