#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_pitanja.h"

Odgovor_Pitanja::Odgovor_Pitanja()
{
    Tip=ODGOVOR_PITANJA;
}

Odgovor_Pitanja::~Odgovor_Pitanja()
{
    listaPitanja.clear();
}

QByteArray Odgovor_Pitanja::za_slanje()
{
    // tip || br_p || pitanja
    using namespace Fje_za_Koriscenje;

    QString stringZaSlanje;
    stringZaSlanje.append(enkapsuliraj(QString::number(Tip)));
    int br_p=listaPitanja.length();
    stringZaSlanje.append(enkapsuliraj(QString::number(br_p)));

    for( int i=0; i<br_p; i++ )
        stringZaSlanje.append(enkapsuliraj(listaPitanja[i].za_slanje()));

    QByteArray polje_za_slanje;
    polje_za_slanje.append(stringZaSlanje);
    return polje_za_slanje;
}

int Odgovor_Pitanja::praviOd(QString& izvorniBitArray)
{
    // tip || br_p || pitanja
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    if( pomocna.toInt()!=ODGOVOR_PITANJA)
        return 0;
    Tip=ODGOVOR_PITANJA;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    int br_p=pomocna.toInt();

    for( int i=0; i<br_p; i++ )
    {
        Pitanje k;
        QString ba;
        ba.append(dekapsulacija(izvorniBitArray,iterator));
        k.pravi_od(ba);
        listaPitanja.append(k);
    }
    return iterator;
}
