#ifndef ODGOVOR_OBLASTI_H
#define ODGOVOR_OBLASTI_H

#include <QObject>
#include <QString>
#include <QList>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

#include "Engine/PPOK/oblast.h"
#include "Engine/PPOK/ppok.h"
#include "Engine/PPOK/pitanje.h"

class Odgovor_Oblasti : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Oblasti();
    ~Odgovor_Oblasti();

    QList<Oblast> vratiListu()    {return listaOblasti;}
    //isto kao i kod kvizovi
    void dodaj_oblast(Oblast &o){
        listaOblasti.append(o);
    }
    void brisi_oblast(Oblast &o){
        int i=listaOblasti.indexOf(o);
        if( i==-1 )
            return;
        listaOblasti.removeAt(i);
    }

    QByteArray  za_slanje();
    int praviOd(QString& izvorniBitArray);

private:
    QList<Oblast> listaOblasti;
};

#endif // ODGOVOR_OBLASTI_H
