#include "Engine/Odgovor_na_komandu/Odgovori_sa_mod_Baze/odgovor_snimljen.h"

Odgovor_Snimljen::Odgovor_Snimljen()
{
    Tip=ODGOVOR_SNIMLJEN;
    OK=true;
    ID_objekta=-1;
}
Odgovor_Snimljen::~Odgovor_Snimljen()
{

}

void Odgovor_Snimljen::postaviOK(bool novo)
{
    OK=novo;
}

bool Odgovor_Snimljen::vratiOK()
{
    return OK;
}

QByteArray  Odgovor_Snimljen::za_slanje()
{
    // TIP || OK || ID
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(Tip)));
    povratniString.append(enkapsuliraj(QString::number(OK?1:0)));
    povratniString.append(enkapsuliraj(QString::number(ID_objekta)));

    //std::cout<<povratniString.toStdString()<<std::endl;
    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}

int Odgovor_Snimljen::praviOd(QString& izvorniBitArray)
{

    // TIP || OK || ID
        using namespace Fje_za_Koriscenje;

    QString izvorniQString="";
    izvorniQString.append(izvorniBitArray);
    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=ODGOVOR_SNIMLJEN)
        return 0;

    //dekapsulacija
    pomocna=dekapsulacija(izvorniQString,iterator);
    postaviOK(pomocna.compare("1")==0?1:0);

    pomocna=dekapsulacija(izvorniQString,iterator);
    ID_objekta=pomocna.toInt();

    return iterator;
}
