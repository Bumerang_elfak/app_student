#ifndef ODGOVOR_SNIMLJEN_H
#define ODGOVOR_SNIMLJEN_H

#include <QObject>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

class Odgovor_Snimljen : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Snimljen();
    ~Odgovor_Snimljen();

    void postaviOK(bool novo);
    bool vratiOK();


    void postavi_ID( int n ){
        ID_objekta=n;
    }
    int vrati_ID(){
        return ID_objekta;
    }


    QByteArray  za_slanje();
    int praviOd(QString& izvorniBitArray);

private:
    int ID_objekta;

};

#endif // ODGOVOR_SNIMLJEN_H
