#ifndef ODGOVOR_MODIFIKOVAN_H
#define ODGOVOR_MODIFIKOVAN_H

#include <QObject>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/fje_za_koriscenje.h"

class Odgovor_Modifikovan : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Modifikovan();
    ~Odgovor_Modifikovan();

    void postaviOK(bool novo);
    bool vratiOK();

    QByteArray  za_slanje();
    int praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_MODIFIKOVAN_H
