#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

Odgovor_na_Komandu::Odgovor_na_Komandu(QObject *parent) : QObject(parent){
    OK=true;
}
Odgovor_na_Komandu::~Odgovor_na_Komandu(){

}
void Odgovor_na_Komandu::salji_na(QTcpSocket* sock){
    QByteArray podaci=za_slanje();
    int duzina_pod=podaci.size();

    sock->write((char *)&duzina_pod,4);
    sock->write(podaci);
}

