#ifndef ERROR_DISKONEKTOVAN_SA_SERVERA_H
#define ERROR_DISKONEKTOVAN_SA_SERVERA_H

#include <QObject>
#include "Engine/Enums.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/Odgovor_na_komandu/error.h"

class error_diskonektovan_sa_servera : public Error
{
public:
    error_diskonektovan_sa_servera();
    //ne trebaju mu nikakvi parametri, samo se postavlja greske, sve ostalo ima u roditelju
};

#endif // ERROR_DISKONEKTOVAN_SA_SERVERA_H
