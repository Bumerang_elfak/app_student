#ifndef ODGOVOR_ZAVRSEN_KVIZ_H
#define ODGOVOR_ZAVRSEN_KVIZ_H

#include <QObject>
#include "Engine/PPOK/kviz.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class Odgovor_Uspesno_zavrsen_Kviz : public Odgovor_na_Komandu
{
    Q_OBJECT
    Kviz k;

public:
    Odgovor_Uspesno_zavrsen_Kviz();

    void postavi_kviz( Kviz k ){
        this->k=k;
    }
    Kviz vrati_kviz(){
        return k;
    }

    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_ZAVRSEN_KVIZ_H
