#include "odgovor_generisi_kviz_dinamicki.h"

odgovor_generisi_kviz_dinamicki::odgovor_generisi_kviz_dinamicki()
{
    Tip=ODGOVOR_GENERISI_KVIZ_DINAMICKI;
}

odgovor_generisi_kviz_dinamicki::~odgovor_generisi_kviz_dinamicki()
{
}


QByteArray odgovor_generisi_kviz_dinamicki::za_slanje()
{
    // TIP || br_p || Pitanja
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    int br_p=listaPitanja.length();
    s.append(enkapsuliraj(QString::number(br_p)));

    for(int i=0;i<br_p;i++)
    {
        s.append(enkapsuliraj(listaPitanja[i].za_slanje()));
    }

    QByteArray ba;
    ba.append(s);
    return ba;
}

int odgovor_generisi_kviz_dinamicki::praviOd(QString& izvorniStringArray)
{
    // TIP || br_p || Pitanja
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(izvorniStringArray,iterator);
    if(pomocna.toInt()!=ODGOVOR_GENERISI_KVIZ_DINAMICKI )
        return 0;

    int br_p=(dekapsulacija(izvorniStringArray,iterator)).toInt();

    for(int i=0;i<br_p;i++)
    {
        Pitanje temp;
        QString ba;
        ba.append(dekapsulacija(izvorniStringArray,iterator));
        temp.pravi_od(ba);
        listaPitanja.append(temp);
    }
    return iterator;
    //dodat red, obrisi
}
