#include "odgovor_nasilan_prekid.h"

Odgovor_nasilan_Prekid::Odgovor_nasilan_Prekid()
{
    Tip=ODGOVOR_KVIZ_NASILAN_PREKID;
}
QByteArray  Odgovor_nasilan_Prekid::za_slanje(){
    //TIP
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int         Odgovor_nasilan_Prekid::praviOd(QString& s){
    //TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_ISTEKLO_VREME )
        return 0;
    return iterator;
}
