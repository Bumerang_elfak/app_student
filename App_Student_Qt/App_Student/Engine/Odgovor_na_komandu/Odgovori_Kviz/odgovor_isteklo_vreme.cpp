#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_isteklo_vreme.h"

Odgovor_Isteklo_Vreme::Odgovor_Isteklo_Vreme(){
    Tip=ODGOVOR_KVIZ_ISTEKLO_VREME;
}
QByteArray  Odgovor_Isteklo_Vreme::za_slanje(){
    // TIP
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int        Odgovor_Isteklo_Vreme::praviOd(QString& s){
    //TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_ISTEKLO_VREME )
        return 0;
    return iterator;
}
