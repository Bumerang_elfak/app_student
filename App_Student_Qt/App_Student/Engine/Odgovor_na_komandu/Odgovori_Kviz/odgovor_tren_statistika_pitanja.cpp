#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_tren_statistika_pitanja.h"

Odgovor_tren_statistika_pitanja::Odgovor_tren_statistika_pitanja(){
    Tip=ODGOVOR_KVIZ_TREN_STATISTIKA_PITANJA;
}
QByteArray  Odgovor_tren_statistika_pitanja::za_slanje(){

    //TIP || BR_TACNIH || BR_NE_TACNIH || UKUPNO
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(br_tacnih)));
    s.append(enkapsuliraj(QString::number(br_ne_tacnih)));
    s.append(enkapsuliraj(QString::number(ukupno)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int       Odgovor_tren_statistika_pitanja:: praviOd(QString& s){

    //TIP || BR_TACNIH || BR_NE_TACNIH || UKUPNO
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_TREN_STATISTIKA_PITANJA )
        return 0;

    pomocna=dekapsulacija(s,iterator);
    br_tacnih=pomocna.toInt();

    pomocna=dekapsulacija(s,iterator);
    br_ne_tacnih=pomocna.toInt();

    pomocna=dekapsulacija(s,iterator);
    ukupno=pomocna.toInt();

    return iterator;
}
