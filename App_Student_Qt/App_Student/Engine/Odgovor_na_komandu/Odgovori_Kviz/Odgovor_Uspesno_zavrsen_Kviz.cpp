#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/Odgovor_Uspesno_zavrsen_Kviz.h"

Odgovor_Uspesno_zavrsen_Kviz::Odgovor_Uspesno_zavrsen_Kviz(){
    Tip=ODGOVOR_KVIZ_USPESNO_ZAVRSEN_KVIZ;
}
QByteArray  Odgovor_Uspesno_zavrsen_Kviz::za_slanje(){
    //Tip || KVIZ
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(k.za_slanje()));
    QByteArray ba;
    ba.append(s);
    return ba;
}
int        Odgovor_Uspesno_zavrsen_Kviz::praviOd(QString& s){
    //TIP || KVIZ
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_USPESNO_ZAVRSEN_KVIZ )
        return 0;

    QString ba;
    ba.append(dekapsulacija(s,iterator));
    k.pravi_od(ba);

    return iterator;

}
