#ifndef ODGOVOR_USPESNA_PRIJAVA_H
#define ODGOVOR_USPESNA_PRIJAVA_H

#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include <QObject>
#include "Engine/PPOK/kviz.h"

class Odgovor_Uspesna_Prijava : public Odgovor_na_Komandu
{
    Q_OBJECT
    Kviz    kviz_koji_se_radi;

public:
    Odgovor_Uspesna_Prijava();

    void postavi_kviz( Kviz k ){
        kviz_koji_se_radi=k;
    }
    Kviz vrati_kviz(){
        return kviz_koji_se_radi;
    }

    virtual QByteArray  za_slanje();
    virtual int         praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_USPESNA_PRIJAVA_H
