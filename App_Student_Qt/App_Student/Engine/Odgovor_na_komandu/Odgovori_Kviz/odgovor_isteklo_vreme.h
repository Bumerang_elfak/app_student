#ifndef ODGOVOR_ISTEKLO_VREME_H
#define ODGOVOR_ISTEKLO_VREME_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"

class Odgovor_Isteklo_Vreme : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    Odgovor_Isteklo_Vreme();

    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString& izvorniBitArray);
};

#endif // ODGOVOR_ISTEKLO_VREME_H
