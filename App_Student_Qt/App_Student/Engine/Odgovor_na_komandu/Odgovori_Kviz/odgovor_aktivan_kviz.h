#ifndef ODGOVOR_AKTIVAN_KVIZ_H
#define ODGOVOR_AKTIVAN_KVIZ_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"


class Odgovor_Aktivan_Kviz : public Odgovor_na_Komandu
{
    Q_OBJECT
private:
    int ID_Aktivan_Kviz=-1;
    int sifra=-1;

public:
    Odgovor_Aktivan_Kviz();
    ~Odgovor_Aktivan_Kviz();

    void postavi_akt_kviz(int a){
        ID_Aktivan_Kviz=a;
    }
    int vrati_akt_Kviz(){
        return ID_Aktivan_Kviz;
    }

    void postavi_sifra(int sifra){
        this->sifra=sifra;
    }
    int vrati_sifra(){
        return sifra;
    }

    virtual QByteArray  za_slanje();
    virtual int        praviOd(QString& izvorniBitArray);

};

#endif // ODGOVOR_AKTIVAN_KVIZ_H
