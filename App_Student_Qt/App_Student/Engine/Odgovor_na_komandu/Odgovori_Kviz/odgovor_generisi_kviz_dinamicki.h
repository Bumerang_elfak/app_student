#ifndef ODGOVOR_GENERISI_KVIZ_DINAMICKI_H
#define ODGOVOR_GENERISI_KVIZ_DINAMICKI_H

#include <QObject>
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/Enums.h"
#include "Engine/PPOK/pitanje.h"
#include <QList>

class odgovor_generisi_kviz_dinamicki : public Odgovor_na_Komandu
{
    Q_OBJECT
public:
    odgovor_generisi_kviz_dinamicki();
    ~odgovor_generisi_kviz_dinamicki();
//asd
    QByteArray za_slanje();
    int praviOd(QString& izvorniBitArray);

    QList<Pitanje>  vrati_listu_pitanja(){
        return listaPitanja;
    }
    void dodajPitanje(Pitanje &novoPitanje){
        listaPitanja.append(novoPitanje);
    }

private:
    QList<Pitanje> listaPitanja;
};

#endif // ODGOVOR_GENERISI_KVIZ_DINAMICKI_H
