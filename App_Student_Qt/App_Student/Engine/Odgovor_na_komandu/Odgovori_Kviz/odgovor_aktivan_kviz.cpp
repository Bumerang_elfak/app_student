#include "Engine/Odgovor_na_komandu/Odgovori_Kviz/odgovor_aktivan_kviz.h"

Odgovor_Aktivan_Kviz::Odgovor_Aktivan_Kviz(){
    Tip=ODGOVOR_KVIZ_AKTIVAN_KVIZ;
}
Odgovor_Aktivan_Kviz::~Odgovor_Aktivan_Kviz(){}
QByteArray  Odgovor_Aktivan_Kviz::za_slanje(){
    //TIP || ID_AKT_KVIZA || sifra
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    s.append(enkapsuliraj(QString::number(ID_Aktivan_Kviz)));
    s.append(enkapsuliraj(QString::number(sifra)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int        Odgovor_Aktivan_Kviz::praviOd(QString& s){
    //TIP || ID_AKT_KVIZA || sifra
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(s,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_AKTIVAN_KVIZ )
        return 0;

    pomocna=dekapsulacija(s,iterator);
    ID_Aktivan_Kviz=pomocna.toInt();

    pomocna=dekapsulacija(s,iterator);
    sifra=pomocna.toInt();

    return iterator;
}
