#include "odgovor_cekaj_sledece_pitanje.h"

odgovor_cekaj_sledece_pitanje::odgovor_cekaj_sledece_pitanje()
{
    Tip=ODGOVOR_KVIZ_CEKAJ_SLEDECE_PITANJE;
}

QByteArray  odgovor_cekaj_sledece_pitanje::za_slanje(){
    // TIP
    using namespace Fje_za_Koriscenje;

    QString s;
    s.append(enkapsuliraj(QString::number(Tip)));
    QByteArray ba;
    ba.append(s);
    return ba;

}
int        odgovor_cekaj_sledece_pitanje::praviOd(QString& izvorniBitArray){
    //TIP
    using namespace Fje_za_Koriscenje;

    int iterator=0;
    QString pomocna;

    pomocna=dekapsulacija(izvorniBitArray,iterator);
    if(pomocna.toInt()!=ODGOVOR_KVIZ_CEKAJ_SLEDECE_PITANJE )
        return 0;

    return iterator;
}
