#include "Engine/Osobe/profesor.h"

Profesor::Profesor()
{
    tipOsobe=PROFESOR;
}
Profesor::Profesor(const Profesor &profesor)
    :Osobe(profesor)
{
    tipOsobe=PROFESOR;
    Id=profesor.Id;
    Ime=profesor.Ime;
    Prezime=profesor.Prezime;
    E_mail=profesor.E_mail;
    Username=profesor.Username;
    Password=profesor.Password;
}
Profesor::~Profesor()
{

}

Profesor:: Profesor(int idprofesora, QString ime,QString prezime, QString email, QString username, QString password)
{
    Id=idprofesora;
    Ime=ime;
    Prezime=prezime;
    E_mail=email;
    Username=username;
    Password=password;
}


void Profesor::promeniIme(QString novo)
{
    Ime=novo;
}
void Profesor::promeniPrezime(QString novo)
{
    Prezime=novo;
}
void Profesor::promeniEmail(QString novo)
{
    E_mail=novo;
}


QByteArray Profesor::za_slanje()
{
    // tipOsobe || ID || ime || Prezime || E_mail || Username || Password
    using namespace Fje_za_Koriscenje;

    //dodavanje atributa
    QString povratniString;
    povratniString.append(enkapsuliraj(QString::number(tipOsobe)));
    povratniString.append(enkapsuliraj(QString::number(Id)));
    povratniString.append(enkapsuliraj(Ime));
    povratniString.append(enkapsuliraj(Prezime));
    povratniString.append(enkapsuliraj(E_mail));
    povratniString.append(enkapsuliraj(Username));
    povratniString.append(enkapsuliraj(Password));

    //prevodjenje u bitarray
    QByteArray poljeZaSlanje;
    poljeZaSlanje.append(povratniString);
    return poljeZaSlanje;
}

int Profesor::pravi_od(QString &izvorniQString)
{
    using namespace Fje_za_Koriscenje;
    // tipOsobe || ID || ime || Prezime || E_mail || Username || Password

    int iterator=0;
    QString pomocna;
    pomocna=dekapsulacija(izvorniQString,iterator);
    if(pomocna.toInt()!=PROFESOR)
        return 0;

    //dekapsulacija
    tipOsobe=PROFESOR;
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniId(pomocna.toInt());
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniIme(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPrezime(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniEmail(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniUsername(pomocna);
    pomocna=dekapsulacija(izvorniQString,iterator);
    promeniPassword(pomocna);

    return iterator;
}

Profesor& Profesor::operator=(const Profesor& profesor)
{
    Id=profesor.Id;
    Ime=profesor.Ime;
    Prezime=profesor.Prezime;
    E_mail=profesor.E_mail;
    Username=profesor.Username;
    Password=profesor.Password;
    return *this;
}

bool Profesor::operator==(const Profesor& profesor)
{
    if(Id!=profesor.Id)
        return false;
    if(Ime.compare(profesor.Ime)==0)
        return false;
    if(Prezime.compare(profesor.Prezime)==0)
        return false;
    if(E_mail.compare(profesor.E_mail)==0)
        return false;
    if(Username.compare(profesor.Username)==0)
        return false;
    if(Password.compare(profesor.Password)==0)
        return false;

    return true;
}
