#ifndef ADMIN_H
#define ADMIN_H

#include "Engine/Osobe/osobe.h"
#include "Engine/fje_za_koriscenje.h"
#include <QString>

class Admin : public Osobe
{
    Q_OBJECT

public:
    Admin(QObject *parent=0);
    Admin(const Admin &admin);
    ~Admin();
    Admin(int idadmina, QString ime,QString prezime, QString email, QString username, QString password);


    void promeniIme(QString novo);
    void promeniPrezime(QString novo);
    void promeniEmail(QString novo);

    QString vrati_Ime(){
        return Ime;
    }
    QString vrati_Prezime(){
        return Prezime;
    }
    QString vrati_Email(){
        return E_mail;
    }


    Admin& operator=(const Admin& admin);
    bool operator==(const Admin& admin);

    QByteArray za_slanje();
    int pravi_od(QString &izvorniBitArray);

private:

    QString Ime="";
    QString Prezime="";
    QString E_mail="";

};

#endif // ADMIN_H
