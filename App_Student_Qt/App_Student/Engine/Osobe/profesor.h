#ifndef PROFESOR_H
#define PROFESOR_H


#include <QString>
#include "Engine/Osobe/osobe.h"
#include "Engine/fje_za_koriscenje.h"

class Profesor : public Osobe
{
    Q_OBJECT

public:
    Profesor();
    Profesor(const Profesor& profesor);
    ~Profesor();
     Profesor(int idaprofesora, QString ime,QString prezime, QString email, QString username, QString password);

    void promeniIme(QString novo);
    void promeniPrezime(QString novo);
    void promeniEmail(QString novo);

    QString vrati_Ime(){
        return Ime;
    }
    QString vrati_Prezime(){
        return Prezime;
    }
    QString vrati_Email(){
        return E_mail;
    }


    Profesor& operator=(const Profesor& profesor);
    bool operator==(const Profesor& profesor);

    QByteArray za_slanje();
    int pravi_od(QString &izvorniBitArray);

private:

    QString Ime;
    QString Prezime;
    QString E_mail;

};

#endif // PROFESOR_H
