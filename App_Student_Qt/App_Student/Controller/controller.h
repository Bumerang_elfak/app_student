#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QTimer>

#include "Engine/komunikacija_sa_serverom.h"
#include "Engine/Osobe/profesor.h"
#include "Engine/PPOK/predmet.h"
#include "Engine/Komanda/komanda_logovanje.h"
#include "Engine/Odgovor_na_komandu/Odgovori_sa_Podacima/odgovor_logovanje.h"
#include "Engine/Komanda/Komanda.h"
#include "Engine/Odgovor_na_komandu/odgovor_na_komandu.h"
#include "Engine/PPOK/predmet.h"
#include "Engine/Osobe/osobe.h"
#include "Engine/Osobe/profesor.h"
#include "Engine/PPOK/oblast.h"
#include "Engine/Komanda/Komanda_nad_bazom/Komanda_nad_bazom_upit/komandaupit_predmet.h"

class Controller : public QObject
{
    Q_OBJECT
public:

    explicit Controller(QObject *parent = 0);
    void postavi_komunikaciju()
    {
        this->komunikacija= Komunikacija_sa_Serverom::vrati_komunikaciju();
        connect(komunikacija,SIGNAL(stigo_odg(Odgovor_na_Komandu*)),
                this,SLOT(primi_odgovor_na_komandu(Odgovor_na_Komandu*)),Qt::QueuedConnection);
    }
    void student_uneo_odgovor(int id);
    Pitanje vrati_pitanje(int id_trenutnog_pitanja);

    void komanda_vrati_pitanja()
    {
        pitanja.clear();
        komandaupit_pitanje* kpitanje= new komandaupit_pitanje();
        komunikacija->salji_komandu(kpitanje);//sva pitanja
    }
    void komanda_odgovorio(bool odgovor);









private:
    QList<Pitanje> pitanja;
    Kviz aktivan_Kviz;
    Komunikacija_sa_Serverom* komunikacija;
    QTimer t;
    bool da_li_je_tajmer_otkucao;


signals:
    void stigo_odg_od_komunikacije(Odgovor_na_Komandu* odg);
    void signal_greska(Error* greska);
    void refresh_logovanje(Odgovor_na_Komandu* odg);
    void signal_error_diskonektovan_sa_servera();
    void signal_sledece_pitanje(Pitanje trenutno);
    void signal_cekaj_sl_pitanje();
    void signal_nasilan_prekid();
    void signal_normalan_prekid();
    void signal_zavrsen_kviz();





public slots:

    void primi_odgovor_na_komandu(Odgovor_na_Komandu* kom);
};

#endif // CONTROLLER_H
