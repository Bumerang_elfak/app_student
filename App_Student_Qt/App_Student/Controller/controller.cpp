#include "controller.h"

Controller::Controller(QObject *parent) : QObject(parent)
{
    //t.setInterval(30000);
    //t.setSingleShot(true);
    //connect(&t,SIGNAL(timeout()),this,SLOT(otkucao_tajmer()));
    //da_li_je_tajmer_otkucao=true;

}
void Controller::primi_odgovor_na_komandu(Odgovor_na_Komandu *kom)
{

    switch( kom->vrati_tip())
    {
        case ODGOVOR_KVIZ_USPESNA_PRIJAVA:
        {
            Odgovor_Uspesna_Prijava* odg=qobject_cast<Odgovor_Uspesna_Prijava*>(kom);
            this->aktivan_Kviz=odg->vrati_kviz();
            this->pitanja.clear();
            this->pitanja=aktivan_Kviz.vrati_pitanja();
            emit refresh_logovanje(kom);
            break;
        }
        case ODGOVOR_KVIZ_SLEDECE_PITANJE:
        {
          Odgovor_sledece_Pitanje* odg=qobject_cast<Odgovor_sledece_Pitanje*>(kom);
          int id_trenutnog_pitanja=odg->vrati_sledece_pitanje();
          if(id_trenutnog_pitanja!=-1)
          {
          Pitanje trenutno=this->vrati_pitanje(id_trenutnog_pitanja);
          emit signal_sledece_pitanje(trenutno);
          }
          break;
        }
        case ODGOVOR_KVIZ_CEKAJ_SLEDECE_PITANJE:
        {
            emit signal_cekaj_sl_pitanje();
            break;
        }
        case ODGOVOR_KVIZ_NASILAN_PREKID:
        {
            emit signal_nasilan_prekid();
            break;

        }
        case ODGOVOR_KVIZ_USPESNO_ZAVRSEN_KVIZ:
        {
          //da se pita
            emit signal_normalan_prekid();
            break;
        }
        case ERROR:
        {
            Error* greska=qobject_cast<Error*>(kom);
            emit signal_greska(greska);
            break;
        }
        case ERROR_DISKONEKTOVAN_SA_SERVERA:
        {
            //sve forme koje su trenutno aktivne da se zatvore, da se pobrisu predmeti(ceo model)
            //i da se otvori Forma Logovanje i da se izbaci greksa
            emit signal_error_diskonektovan_sa_servera();
            break;
        }
        default:
        {
            Error* greska=new Error();
            greska->postavi_greska("Dosao je odgovor ali ne umem da ga razumem");
            emit signal_greska(greska);
            break;
        }
    }
    delete kom;
}

void Controller::student_uneo_odgovor(int id)
{
    Komanda_Kviz_Log* klog=new Komanda_Kviz_Log();
    klog->postavi_sifru_kviza(id);
    komunikacija->salji_komandu(klog);
}
Pitanje Controller::vrati_pitanje(int id_trenutnog_pitanja)
{
    for(int i=0; i<pitanja.count();i++)
    {
        if(pitanja[i].vrati_ID()==id_trenutnog_pitanja)
        {
            return (pitanja[i]);
        }
    }
}
void Controller::komanda_odgovorio(bool odgovor)
{
    Komanda_Kviz_Odg_Pitanje* kodgovor=new Komanda_Kviz_Odg_Pitanje();
    kodgovor->postavi_ID_Aktivan_kviz(this->aktivan_Kviz.vrati_ID());
    kodgovor->postavi_tacan(odgovor);
    komunikacija->salji_komandu(kodgovor);
}
