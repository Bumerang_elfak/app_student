#ifndef SACEKAJTEPITANJE_H
#define SACEKAJTEPITANJE_H
#include "zavrsenkviz.h"
#include "View/view2.h"
#include <QDialog>

class RadiKviz;

namespace Ui {
class SacekajtePitanje;
}

class SacekajtePitanje : public view2
{
    Q_OBJECT

public:
    explicit SacekajtePitanje(QWidget *parent = 0);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
        connect(this->controller,SIGNAL(signal_error_diskonektovan_sa_servera()),this,SLOT(odgovor_diskonektovan()));
        connect(this->controller,SIGNAL(signal_sledece_pitanje(Pitanje)),this,SLOT(Odgovor_sledece_Pitanje(Pitanje)));
        connect(controller, SIGNAL(signal_greska(Error*)),this,SLOT(odgovor_error(Error*)));
        connect(this->controller,SIGNAL(signal_nasilan_prekid()),this,SLOT(odgovor_nasilan_prekid()));
        connect(this->controller,SIGNAL(signal_normalan_prekid()),this,SLOT(odgovor_normalan_prekid()));


    }
    ~SacekajtePitanje();

private:
    Ui::SacekajtePitanje *ui;
    RadiKviz *radiKviz;
    ZavrsenKviz* normalan_zavrsetak;
    ZavrsenKviz* nasilan_zavrsetak;


public slots:
    void Odgovor_sledece_Pitanje(Pitanje trenutno);
    void odgovor_diskonektovan();
    void odgovor_error(Error* greska);
    void odgovor_nasilan_prekid();
    void odgovor_normalan_prekid();





};

#endif // SACEKAJTEPITANJE_H
