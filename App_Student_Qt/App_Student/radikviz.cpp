#include "radikviz.h"
#include "ui_radikviz.h"

RadiKviz::RadiKviz(QWidget *parent) :
    view2(parent),
    ui(new Ui::RadiKviz)
{
    ui->setupUi(this);
    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle( tr("Kviz Bumerang"));
}

RadiKviz::~RadiKviz()
{
    delete ui;
}
void RadiKviz::postavi_formu()
{
    QString tekst=this->tekuce_pitanje.vrati_tekst();
    ui->tekstPitanjalabel->setText(tekst);
    QList<QString> odgovori=tekuce_pitanje.vrati_odgovore();
    ui->odg1Edit->setText(odgovori[0]);
    ui->odg2Edit->setText(odgovori[1]);
    ui->odg3Edit->setText(odgovori[2]);
    ui->odg4Edit->setText(odgovori[3]);
}

void RadiKviz::on_nextButton_clicked()
{
    int brojTacnog=this->tekuce_pitanje.vrati_tacan();
    if(ui->odg1Button->isChecked())
    {
        if(brojTacnog==1)
            this->controller->komanda_odgovorio(true);
        else
            this->controller->komanda_odgovorio(false);
    this->close();
    spitanje=new SacekajtePitanje();
    spitanje->AddListener(this->controller);
    spitanje->show();
    this->deleteLater();

    }
    else if(ui->odg2Button->isChecked())
    {
        if(brojTacnog==2)
            this->controller->komanda_odgovorio(true);
        else
            this->controller->komanda_odgovorio(false);
        this->close();
        spitanje=new SacekajtePitanje();
        spitanje->AddListener(this->controller);
        spitanje->show();
        this->deleteLater();
    }
    else if(ui->odg3Button->isChecked())
    {
        if(brojTacnog==3)
            this->controller->komanda_odgovorio(true);
        else
            this->controller->komanda_odgovorio(false);
        this->close();
        spitanje=new SacekajtePitanje();
        spitanje->AddListener(this->controller);
        spitanje->show();
        this->deleteLater();
    }
    else if(ui->odg4Button->isChecked())
    {
        if(brojTacnog==4)
            this->controller->komanda_odgovorio(true);
        else
            this->controller->komanda_odgovorio(false);
        this->close();
        spitanje=new SacekajtePitanje();
        spitanje->AddListener(this->controller);
        spitanje->show();
        this->deleteLater();
    }
    else
    {
        QMessageBox::warning(this,"Kviz Bumernag","Niste izabrali pitanje",QMessageBox::Ok,QMessageBox::NoButton);
        return;
        this->setVisible(true);
    }
}
void RadiKviz::odgovor_diskonektovan()
{
    this->close();
    QMessageBox::warning(this,"Kviz Bumerang","Error - diskonektovan sa servera!",QMessageBox::Ok,QMessageBox::NoButton);
    this->deleteLater();

}
void RadiKviz::odgovor_error(Error *greska)
{
    QString naziv_greska=greska->vrati_greska();
    qDebug()<<naziv_greska;
    QMessageBox::warning(this,"Kviz Bumerang",naziv_greska,QMessageBox::Ok,QMessageBox::NoButton);
    return;
}
void RadiKviz::odgovor_cekaj_sl_pitanje()
{
    this->close();
    spitanje=new SacekajtePitanje();
    spitanje->AddListener(this->controller);
    spitanje->show();
    this->deleteLater();
}
void RadiKviz::odgovor_nasilan_prekid()
{
    this->close();
    //QMessageBox::warning(this,"Kviz Bumerang","Profesor je prekinuo kviz!",QMessageBox::Ok,QMessageBox::NoButton);
    nasilan_zavrsetak=new ZavrsenKviz();
    nasilan_zavrsetak->postavi_formu("Profesor je prekinuo kiviz!");
    nasilan_zavrsetak->show();
    this->deleteLater();
}

