#include "studentunosid.h"
#include "ui_studentunosid.h"
#include <QFileDialog>
#include <QTimer>
#include <QMessageBox>
#include <QDebug>
#include <QWidget>
#include <QStandardItem>
#include <QAbstractItemModel>
#include <QAbstractItemView>


StudentUnosID::StudentUnosID(QWidget *parent) :
    View(parent),
    ui(new Ui::StudentUnosID)
{
    ui->setupUi(this);
    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle( tr("Kviz Bumerang"));
}

StudentUnosID::~StudentUnosID()
{
    delete ui;
}

void StudentUnosID::on_zapocniKvizButton_clicked()
{
    if(ui->IDEdit->text().isEmpty())
    {
          QMessageBox::warning(this,"Kviz Bumernag","Niste uneli ID!",QMessageBox::Ok,QMessageBox::NoButton);
          return;
          this->setVisible(true);
    }
    int id=ui->IDEdit->text().toInt();
    this->controller->student_uneo_odgovor(id);

}

void StudentUnosID::odgovor_na_logovanje(Odgovor_na_Komandu *odg)
{
    //ceka se odgovor sl_pitanje
    // QMessageBox::warning(this,"Kviz Bumerang","Uneli ste tacan pin!",QMessageBox::Ok,QMessageBox::NoButton);
    //return;
    sacekajte_pit=new SacekajtePitanje();
    sacekajte_pit->AddListener(this->controller);
    sacekajte_pit->show();
    this->close();
    this->deleteLater();

}

void StudentUnosID::odgovor_error(Error *greska)
{
    QString naziv_greska=greska->vrati_greska();
    qDebug()<<naziv_greska;
    QMessageBox::warning(this,"Kviz Bumerang",naziv_greska,QMessageBox::Ok,QMessageBox::NoButton);
    ui->IDEdit->setText("");
    return;
}

void StudentUnosID::odgovor_diskonektovan()
{
    QMessageBox::warning(this,"Kviz Bumerang","Error - diskonektovan sa servera!",QMessageBox::Ok,QMessageBox::NoButton);
    this->close();
    this->deleteLater();
}

void StudentUnosID::on_IDEdit_returnPressed()
{
    on_zapocniKvizButton_clicked();
}
