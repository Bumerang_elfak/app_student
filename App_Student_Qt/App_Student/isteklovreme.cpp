#include "isteklovreme.h"
#include "ui_isteklovreme.h"

IstekloVreme::IstekloVreme(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IstekloVreme)
{
    ui->setupUi(this);
    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle( tr("Kviz Bumerang"));
}

IstekloVreme::~IstekloVreme()
{
    delete ui;
}
