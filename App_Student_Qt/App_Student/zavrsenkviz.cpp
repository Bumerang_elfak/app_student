#include "zavrsenkviz.h"
#include "ui_zavrsenkviz.h"

ZavrsenKviz::ZavrsenKviz(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ZavrsenKviz)
{
    ui->setupUi(this);
    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle( tr("Kviz Bumerang"));
}

ZavrsenKviz::~ZavrsenKviz()
{
    delete ui;
}
void ZavrsenKviz::postavi_formu(QString naziv)
{
    ui->label->setText(naziv);
}

void ZavrsenKviz::on_pushButton_clicked()
{
    this->close();
    this->deleteLater();
}
