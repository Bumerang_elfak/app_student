#ifndef VIEW_H
#define VIEW_H\

#include "Engine/Komanda/Komanda.h"
#include "Controller/controller.h"
#include <QObject>
#include <QMainWindow>

class View: public QMainWindow
{
    Q_OBJECT
protected:
    Controller *controller;
public:
    explicit View(QWidget *parent=NULL);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
    }

public slots:




};

#endif // VIEW_H
