#ifndef RADIKVIZ_H
#define RADIKVIZ_H
#include "View/view2.h"
#include <QMessageBox>
#include <QDialog>
#include "sacekajtepitanje.h"
#include "zavrsenkviz.h"

namespace Ui {
class RadiKviz;
}

class RadiKviz : public view2
{
    Q_OBJECT

public:
    explicit RadiKviz(QWidget *parent = 0);
    virtual void AddListener(Controller* controller)
    {
        this->controller=controller;
        connect(controller, SIGNAL(signal_greska(Error*)),this,SLOT(odgovor_error(Error*)));
        connect(this->controller,SIGNAL(signal_error_diskonektovan_sa_servera()),this,SLOT(odgovor_diskonektovan()));
        connect(this->controller,SIGNAL(signal_cekaj_sl_pitanje()),this,SLOT(odgovor_cekaj_sl_pitanje()));
        connect(this->controller,SIGNAL(signal_nasilan_prekid()),this,SLOT(odgovor_nasilan_prekid()));


    }
    ~RadiKviz();

private:
    Ui::RadiKviz *ui;
    Pitanje tekuce_pitanje;
    SacekajtePitanje* spitanje;
    ZavrsenKviz* nasilan_zavrsetak;


public slots:
    void postavi_pitanje(Pitanje trenutno)
    {
        this->tekuce_pitanje=trenutno;
    }
    void postavi_formu();
    void odgovor_diskonektovan();
    void odgovor_error(Error* greska);
    void odgovor_cekaj_sl_pitanje();
    void odgovor_nasilan_prekid();


private slots:
    void on_nextButton_clicked();
};

#endif // RADIKVIZ_H
