#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "studentunosid.h"
#include <QFileDialog>
#include <QTimer>
#include <QMessageBox>
#include <QDebug>
#include <QWidget>
#include <QStandardItem>
#include <QAbstractItemModel>
#include <QAbstractItemView>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle( tr("Kviz Bumerang"));
    //QPixmap bkgnd("C:/Users/LolaS/Desktop/slike/retard2.jpg.jpg");
    //bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    //QPalette palette;
    //palette.setBrush(QPalette::Background, bkgnd);
    //this->setPalette(palette);
    //this->setFixedSize(500,500);
    ui->passwordEdit->setEchoMode(QLineEdit::Password);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loginButton_clicked()
{
    if(ui->usernameEdit->text().isEmpty() && ui->passwordEdit->text().isEmpty())
    {  QMessageBox::warning(this,"Kviz Bumernag","Niste uneli podatke! Unesite username i password.",QMessageBox::Ok,QMessageBox::NoButton);
        return;
    }
    if(ui->usernameEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Unesite username!",QMessageBox::Ok,QMessageBox::NoButton);
        ui->usernameEdit->setText("");
        ui->passwordEdit->setText("");
        return;
    }
    if(ui->passwordEdit->text().isEmpty())
    {
        QMessageBox::warning(this,"Kviz Bumernag","Unesite password!",QMessageBox::Ok,QMessageBox::NoButton);
        ui->usernameEdit->setText("");
        ui->passwordEdit->setText("");
        return;
    }
    QString username=ui->usernameEdit->text();
    QString password=ui->passwordEdit->text();

    bool TacnoSve=true;
    if(TacnoSve)
    {
        this->close();
        suID=new StudentUnosID(this);
        suID->show();
    }

}
