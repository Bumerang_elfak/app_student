#ifndef ZAVRSENKVIZ_H
#define ZAVRSENKVIZ_H

#include <QDialog>

namespace Ui {
class ZavrsenKviz;
}

class ZavrsenKviz : public QDialog
{
    Q_OBJECT

public:
    explicit ZavrsenKviz(QWidget *parent = 0);
    ~ZavrsenKviz();
    void postavi_formu(QString naziv);

private slots:
    void on_pushButton_clicked();

private:
    Ui::ZavrsenKviz *ui;
};

#endif // ZAVRSENKVIZ_H
