#include "sacekajtepitanje.h"
#include "ui_sacekajtepitanje.h"
#include "radikviz.h"


SacekajtePitanje::SacekajtePitanje(QWidget *parent) :
    view2(parent),
    ui(new Ui::SacekajtePitanje)
{
    ui->setupUi(this);
    QIcon ikonica(":/Slike/Ikonica.ico");
    this->setWindowIcon(ikonica);
    this->setWindowTitle( tr("Kviz Bumerang"));
}

SacekajtePitanje::~SacekajtePitanje()
{
    delete ui;
}
 void SacekajtePitanje::odgovor_diskonektovan()
 {
     this->close();
     QMessageBox::warning(this,"Kviz Bumerang","Error - diskonektovan sa servera!",QMessageBox::Ok,QMessageBox::NoButton);
     this->deleteLater();

 }

void SacekajtePitanje::Odgovor_sledece_Pitanje(Pitanje trenutno)
{
        radiKviz=new RadiKviz();
        radiKviz->postavi_pitanje(trenutno);
        radiKviz->AddListener(this->controller);
        radiKviz->postavi_formu();
        radiKviz->show();
        this->close();
        this->deleteLater();
}
void SacekajtePitanje::odgovor_error(Error *greska)
{
    QString naziv_greska=greska->vrati_greska();
    qDebug()<<naziv_greska;
    QMessageBox::warning(this,"Kviz Bumerang",naziv_greska,QMessageBox::Ok,QMessageBox::NoButton);
    return;
}
void SacekajtePitanje::odgovor_nasilan_prekid()
{
    this->close();
    //QMessageBox::warning(this,"Kviz Bumerang","Profesor je prekinuo kviz!",QMessageBox::Ok,QMessageBox::NoButton);
    nasilan_zavrsetak=new ZavrsenKviz();
    nasilan_zavrsetak->postavi_formu("Profesor je prekinuo kiviz!");
    nasilan_zavrsetak->show();
    this->deleteLater();

}
void SacekajtePitanje::odgovor_normalan_prekid()
{
    this->close();
  //  QMessageBox::warning(this,"Kviz Bumerang","Uspesno ste zavrsili kviz!",QMessageBox::Ok,QMessageBox::NoButton);
    normalan_zavrsetak=new ZavrsenKviz();
    normalan_zavrsetak->postavi_formu("Uspesno ste zavrsili kviz!");
    normalan_zavrsetak->show();
    this->deleteLater();

}
